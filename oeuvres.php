<?php session_start();?>

<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8" />
	<?php include 'Z_meta_variations.php';?>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="robots" content="noindex, nofollow,noarchive">
	<link rel="stylesheet" href="style_main.css" />
	<link rel="stylesheet" href="style_commun.css" />
	<link rel="stylesheet" href="style_responsive.css" />
	<link rel="stylesheet" href="<?php echo var_style(); ?>" />
	<link rel="icon" href="Litote-logo4-rose.png" />
	<title>Litote - Œuvres</title>
</head>

<body>

<?php include 'Z_fonctions_variations.php';?>
<?php include 'Z_connexion.php';?>
<?php include 'Z_updates_bdd.php';?>
<?php include 'Z_fonctions_transversales.php';?>

<div class="container">

<?php menu(var_menu_oeuvres()); ?>

<div class="corps">

<?php

// S'il y a bien une œuvre qui a été demandée ******************************************************

if(isset($_GET['oeuvre']) || isset($_POST['oeuvre']))
{	
	// Vérifier qu'il n'y a pas d'entourloupe, préparer les variables ******************************
	
	$_GET['oeuvre'] = htmlspecialchars($_GET['oeuvre']);
	$titre = isset($_GET['oeuvre']) ? $_GET['oeuvre'] : $_POST['oeuvre'];	
	$oeuvre = array();
	
	// Récupérer les données de l'œuvre ************************************************************
			
	$reponse = $bdd->prepare('SELECT * FROM oeuvres WHERE ID_oeuvre = ?');
	$reponse->execute(array(secu($titre, 'quote')));
	
	while($donnees = $reponse->fetch(PDO::FETCH_ASSOC))
	{
		// Une couleur si c'est du corpus primaire, une autre si c'est hors corpus ****************
		$corpus = $donnees['corpus'] == 1 ? '&#10003;' : '&#10007;';
		
		// Prévoir d'ajuster s'il y a plusieurs autrices *******************************************
		$autrice = check_multi_auteurs($donnees);
		
		// Affichage des données de l'œuvre ********************************************************
		echo '<div class="oeuvre">
	
		<div class="titre">
			<p><strong><i>' . $donnees['titre'] . '</i>, ' . $autrice . ' - Corpus ' . $corpus . '</strong></p>
		</div>
		
		<form class="modif" method="get" action="modif.php">
				<input type="hidden" name="modif_oeuvre" value="' . $donnees['ID_oeuvre'] . '">
				<input type="submit" value="Modifier">
		</form>
		
		<div class="bloc">';
		
		echo '<div class="origine">
				<p><strong>Publication originale</strong> : ' 
				. $donnees['origine_date'] . ', ' 
				. $donnees['origine_edition'] . ', '
				. $donnees['origine_lieu'] . '</p>
			</div>
			
			<div class="courant">
				<p><strong>Édition consultée</strong> : ' 
				. $donnees['courant_date'] . ', ' 
				. $donnees['courant_edition'] . ', '
				. $donnees['courant_lieu'] . '</p>
			</div>
			
			<div class="origine">
				<p><strong>Genre identifié : </strong>' . $donnees['genre_litt'] . '</p>
			</div>';		

		echo var_champs_oeuvres_spec($donnees);
		
		echo '<div class="reflexions">
			<p><strong>Notes en vrac : </strong></p>'
				. $donnees['reflexions'] . '
			</div>
			
			<div class="resume">
				<p><strong>Résumé : </strong></p>'
				. $donnees['resume'] . '
			</div>
			
		</div>
		
		<div class="renvoi">
			<form method="post" action="' . var_index_demo() . '">
				<input type="hidden" name="titre" value="' . $donnees['titre'] . '">
				<input type="submit" value="Voir les citations de cette œuvre">
			</form>
		</div>
	
	</div>';
	}
	$reponse->closeCursor();	
	
	echo '<p><a href="' . var_index_demo() . '">Retourner à l\'accueil</a>.</p>';	 	
}
	
//~ // Si on arrive sur la page par erreur *************************************************************

else
{
	echo '<p></p>Aucun livre n\'a été sélectionné.</p>
	
	<div class="formulaire">
	<form method="post" action="#">
		<div class="choix_oeuvre">
		<select name="oeuvre">';
			$reponse = $bdd->query('SELECT nom, prenom, titre FROM oeuvres ORDER BY nom');

			while($donnees = $reponse->fetch(PDO::FETCH_ASSOC))
			{
				echo '<option value="' . $donnees['ID_oeuvre'] . '">'
					. $donnees['prenom'] . ' ' . $donnees['nom'] . ', <i>' . $donnees['titre'] . '</i>
				</option>';
			}
			$reponse->closeCursor();
		echo '</select>
		<input type="submit" value="Chercher">
		</div>
	</form>
	</div>';	
}

?>

</div>

</div>

<?php echo footer(); ?>

</body>

</html>
