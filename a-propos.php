<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8" />
	<?php include 'Z_meta_variations.php';?>
	<meta name="viewport" content="width=device-width" />
	<meta name="robots" content="noindex, nofollow,noarchive"/>
	<link rel="stylesheet" href="style_commun.css" />
	<link rel="stylesheet" href="style_main.css" />
	<link rel="stylesheet" href="<?php echo var_style();?>" />
	<link rel="icon" type="image/x-icon" href="Litote-logo4-rose.png" />
	<title>À propos</title>
</head>

<body>
	
<?php include 'Z_fonctions_variations.php';?>
<?php include 'Z_fonctions_transversales.php';?>

<div class="presentation mode_d_emploi">

<?php

$impressions = '<p class="left">À noter aussi&#8239;: comme sur l\'accueil ou la visualisation du plan, la mise en page du site prévoit une disposition différente des citations selon qu\'elles sont affichées pour un écran, ou pour impression papier. <strong>Pour une impression papier</strong>, menus et critères de recherche disparaissent, ainsi que tous les fonds de couleur associés aux vignettes&#8239;; la taille de la police est réduite&#8239;; les vignettes ne pouvant plus être déroulées sont recadrées en fonction de la longueur réelle de la citation. Cela permet à l\'utilisateurice d\'imprimer des lots de citations en minimisant la quantité d\'encre et de papier utilisée.</p>';

if(isset($_GET['mode-d-emploi']) && $_GET['mode-d-emploi'] == 'index') {
	$notes_accueil = array(
		1 => array('nom' => 'Recherche globale', 
			'contenu' => 'Il faut noter que pour le moment le mode de recherche globale n\'est pas extrêmement souple: il ne peut fonctionner qu\'avec des expressions exactes, ne proposera pas d\'autocomplétion, ne proposera pas de résultat approchant de l\'expression recherchée. C\'est une fonction qui pourrait être améliorée.'), 
		2 => array('nom' => 'Bricolage_utilisateurices',
			'contenu' => 'Un usage que j\'ai parfois eu : faire une recherche globale puis n\'aller voir que les citations extraites des livres de telle ou telle autrice. Par exemple, chercher le mot «&#8239;mère&#8239;» dans l\'ensemble de la base, mais ensuite, grâce aux liens, n\'aller voir que les résultats qui concernent Nicole Brossard. Ou inversement, faire une recherche sur critère puis un ctrl+F dans la page. Autre exemple encore, développer tout ce qui, dans le corpus principal, est étiqueté «&#8239;héritage, histoire&#8239;», et ensuite chercher toutes les occurrences du mot «&#8239;mémoire&#8239;» dans ces citations pré-triées.'), 
		3 => array('nom' => 'genre', 
			'contenu' => 'Soit par l\'autrice, soit par son éditeur, soit par moi à défaut.'),
		4 => array('nom' => 'langue',
			'contenu' => 'En l\'occurrence, donc, l\'étiquette «&#8239;langue&#8239;», qui m\'a servi à regrouper ces occurrences. Mais il faut noter qu\'elle a aussi une fonction thématique, puisque j\'y ai aussi stocké des extraits parlant, par exemple, du rapport ambigu que certaines autrices entretiennent avec les langues étrangères. En pratique, l\'usage est flottant!')
	);
	
	echo '<h2>Mode d\'emploi de la page d\'accueil de Litote</h2>
	
	<h3>Les critères de recherche</h3>
	
	<p class="left">La page d\'accueil de Litote a deux fonctions principales&#8239;: 
	<ol>
		<li>Accéder à l\'<strong>enregistrement</strong> de nouvelles données&#8239;;</li>
		<li>Opérer des <strong>recherches</strong> sur la banque de citations globale.</li>
	</ol></p>
			
	<p class="left">Pour l\'enregistrement de nouvelles données, c\'est simple&#8239;: au travers du menu, on peut choisir d\'augmenter la base en enregistrant soit de nouvelles œuvres, soit de nouvelles citations. Le bouton «&#8239;Paramètres&#8239;» à droite du menu permet d\'accéder aux autres options d\'enregistrement (tags, chapitres, projets...).</p> 
	
	<p class="left">Pour entamer des recherches globales dans Litote, plusieurs choix sont possibles, parfois combinables.
	<ul>
		<li class="espacement_vertical"><strong><u>Recherche globale</u></strong>. L\'une des fonctions principales se trouve dans le menu&#8239;: dans la barre de texte laissée vide, à côté du bouton «&#8239;Chercher&#8239;», on peut taper quelques mots que l\'on veut pouvoir retrouver. Si l\'on y tape, par exemple, le mot «&#8239;mémoire&#8239;», toutes les citations enregistrées qui contiennent ce mot seront affichées, ainsi que des liens vers tous les résumés d\'œuvres qui le mentionnent aussi. Même chose pour une expression&#8239;: si l\'on cherche très précisément, par exemple, le syntagme «&#8239;mourir de rire&#8239;», ou bien «&#8239;à défaut, invente&#8239;» la base affichera les quelques citations qui contiennent exactement ces expressions' . ndbp_a($notes_accueil, 1) . '.</li>
		<ul>
		<li style="font-size:0.9em;">Notez&#8239;: ce critère de recherche n\'est pas combinable avec les autres, sauf bricolage manuel des utilisateurices' . ndbp_a($notes_accueil, 2) . '.</li>
		</ul>
		<li class="espacement_vertical"><strong><u>Recherche par données éditoriales</u></strong>. La première ligne, sous le menu principal, permet de sélectionner un lot de citations en fonction du nom de l\'autrice recherchée, du titre de l\'ouvrage, du pays de publication, du genre littéraire attribué' . ndbp_a($notes_accueil, 3) . ', de la maison d\'édition.</li>
		<li class="espacement_vertical"><strong><u>Recherche par étiquettes</u></strong>. Sous la barre des critères de recherche éditoriaux, apparaît le cœur de l\'outil, la liste des étiquettes par lesquelles les citations ont été progressivement classées au fil des enregistrement. Elles sont classées par ordre alphabétique, ici, mais d\'autres projets proposent des classements spécifiques (voir en particulier le modèle proposé pour la thèse de Carla Robison). La plupart sont de nature thématique&#8239;: il est possible de rechercher toutes les citations qui se rapportent, par exemple, au thème de l\'homosexualité, ou bien toutes celles qui parlent de haine de soi. Certaines sont plus d\'ordre formel ou théorique&#8239;: on peut ainsi chercher les citations qui évoquent explicitement un engagement littéraire, ou bien celles qui manifestent un jeu avec les règles de la grammaire' . ndbp_a($notes_accueil, 4) . '. </li>
		<li class="espacement_vertical"><strong><u>Recherche par type de corpus</u></strong>. Dernier élément vraiment utile parmi les différents types de recherche possible, la recherche par corpus «&#8239;primaire&#8239;» ou «&#8239;intermédiaire&#8239;»&#8239;: elle est destinée, en fin de projet plutôt, à restreindre les recherches aux livres les plus importants pour le travail à accomplir.</li>
		<ul>
		<li style="font-size:0.9em;">Notez&#8239;: un code couleur répartit ces distinctions de corpus, de toute façon, à l\'affichage général, pour que l\'œil puisse vite identifier les enjeux principaux. Ici, bleu foncé pour le corpus primaire, bleu plus clair pour le corpus intermédiaire&#8239;; les citations du corpus secondaire apparaissent dans la couleur principale, donc à peu près neutre, du site&#8239;; celles qui sont issues de livres hors corpus apparaissent très claires.</li>
		</ul>
	</ul></p>
	
	<p class="left">Tous les critères de recherche ne sont pas combinables (pour le moment...)&#8239;: je l\'ai dit, la recherche globale, en particulier, joue solo.</p>
	
	<p class="left">En revanche, les critères sur données éditoriales et sur étiquettes peuvent <strong>s\'associer les uns aux autres</strong>. La plupart, et par défaut pour tous, sur le mode logique du «&#8239;et&#8239;». Par exemple, il est possible de chercher des citations qui soient&#8239;: 1. issues de livres écrits par Nicole Brossard (critère «&#8239;Autrice&#8239;»), 2. mais seulement parmi ceux qui sont des recueils de poésie (critère «&#8239;Genre&#8239;»), et 3. à condition qu\'elles parlent d\'amour (critère étiquettes). Les critères se combinent alors par restriction progressive, pour mieux cibler les résultats.</p>
	
	<p class="left">Toutefois, il est parfois utile de mobiliser une logique plus inclusive, j\'ai donc prévu de pouvoir activer un «&#8239;tri par «&#8239;ou&#8239;»&#8239;», en haut à gauche des critères de recherche, qui ne vaut que pour les jeux d\'étiquette. Il est par exemple ainsi possible de chercher, parmi les recueils de poésie de Nicole Brossard (critères éditoriaux en logique «&#8239;et&#8239;», cf. plus haut), des citations qui traitent soit de lesbianisme soit de bisexualité (logique «&#8239;ou&#8239;»).</p>
	
	<p class="left">Enfin, il faut noter que d\'autres paramètres de recherche que ceux-ci sont prévus, mais mis à part&#8239;: sur la page du plan de thèse, ainsi que sur la page des projets divers, il est possible de trier les citations en fonction de leur utilisation, prévue ou avérée, dans le plan de thèse ou dans divers projets et brouillons de rédaction.</p><br>
	
	<h3>Les vignettes de citations</h3>
	
	<p class="left">Quels que soient les critères de recherche mobilisés, la page affiche ensuite le lot de citations qui y correspond, par vignettes. Chacune propose différents éléments&#8239;: elle reprend les données éditoriales – doubles&#8239;: celle de l\'édition originale, et celles de l\'éventuelle autre édition que j\'ai pu utiliser –, la citation en elle-même avec sa pagination, les notes que j\'ai pu parfois ajouter.</p>
	
	<p class="left">En haut de la vignette, le titre de l\'œuvre est un lien qui renvoie à son résumé – dans le cadre de cette version démo, j\'ai supprimé les résumés. Il est possible, en cliquant sur «&#8239;modifier&#8239;», de mettre à jour les données de la citation ou bien de les supprimer.</p> 
	
	<p class="left">Le symbole «&#8239;plus&#8239;», en haut à gauche, permet à l\'utilisateurice de stocker des citations dans un panier virtuel, pour pouvoir ensuite enregistrer le lot&#8239;: une méthode efficace pour constituer des brouillons d\'articles ou de communications, ou bien pour agencer progressivement la rédaction de la thèse.</p>
	
	<p class="left">Les dernières lignes proposent un aperçu des différentes étiquettes auxquelles cette citation est associée, et des endroits du plan de thèse où elle a été envisagée, ou bien où elle a effectivement été traitée.</p> 
	
	<p class="left">Par défaut, à l\'ouverture de la page lorsqu\'aucun critère de recherche n\'a encore été choisi par l\'utilisateurice, deux vignettes s\'affichent&#8239;: elles sont choisies aléatoirement dans la banque de citations.</p>'
	
	. $impressions
	
	. fin_page($notes_accueil);
}	

elseif(isset($_GET['mode-d-emploi']) && $_GET['mode-d-emploi'] == 'plan') {
	
	$notes_plan = array(
	
		1 => array ('nom' => 'couleurs',
			'contenu' => 'Le jeu de couleurs prévu est différent de celui de la page d\'accueil, ça n\'est pas crucial mais ça m\'a aidée à savoir quelle fenêtre j\'utilisais exactement aux moments où je rédigeais. Ici, comme sur la page des projets, le corpus principal est identifié en prune foncé, le corpus intermédiaire en violet, le corpus secondaire en prune, l\'éventuel hors corpus en violet clair.')
						
	);
	
	echo '<h2>Mode d\'emploi de la page du plan de thèse</h2>
	
	<p class="left">La page du plan de thèse (ou de mémoire, ou de livre, peu importe – c\'était une thèse pour moi) correspond au projet principal pour lequel Litote a été programmé. Elle permet de visualiser clairement non seulement le plan lui-même, mais aussi les citations qui ont été stockées dans chacun de ses chapitres.</p>
	
	<p class="left">Trois critères de recherche principaux sont donc prévus&#8239;: il est possible</p>
	<ol>
		<li>De voir les citations prévues pour tel ou tel chapitre ;</li>
		<li>De voir les citations réellement utilisées dans tel ou tel chapitre.</li>
		<li>De voir les citations stockées mais pas encore utilisées dans tel ou tel chapitre : extrêmement utile en cours de rédaction.</li>
	</ol>
	J\'ai conservé ici la possibilité de re-trier plus spécifiquement avec critères éditoriaux ou en fonction du corpus' . ndbp_a($notes_plan, 1) . '.</p>
	
	<p class="left">Pour le reste, l\'affichage correspond à ce qui se trouve déjà dans la page d\'accueil. La possibilité, ici aussi, d\'enregistrer des paniers de citations, permet de créer des lots temporaires, qui peuvent en particulier servir de brouillons pour des sous-parties de rédaction.</p>
	
	<p class="left">À noter&#8239;: pour cette version de démonstration, je n\'ai conservé dans la banque de citations que celles qui ont réellement été utilisées quelque part dans ma thèse ou dans mes publications, ce qui réduit assez considérablement la proportion des citations «&#8239;proposées mais non utilisées&#8239;» dans chacun des chapitres.</p>'
	
	. $impressions
	
	. fin_page($notes_plan);
}

elseif(isset($_GET['mode-d-emploi']) && $_GET['mode-d-emploi'] == 'projets') {
	
	$notes_projets = array(
	
		1 => array('nom' => 'session',
			'contenu' => 'Dans le code, il correspond à une variable session, alors que les autres manipulations s\'opèrent avec des extractions directement faites sur la base de données.'),
		2 => array('nom' => 'dates',
			'contenu' => 'Date de la publication, de la journée d\'étude, du colloque ou du séminaire, logiquement, pour les publications et les communications&#8239;; date de la création du brouillon, éventuellement, dans les autres cas.')
		
	);
	
	echo '<h2>Mode d\'emploi de la page des projets</h2>
	
	<p class="left">La page des projets est l\'une des pages les plus utiles de cette banque de citations, en ce qu\'elle permet notamment de <strong>manipuler des regroupements restreints de citations</strong>.</p>
	
	<p class="left">Son but général est de présenter d\'autres coulisses du projet principal, la thèse en l\'occurrence&#8239;: elle montre comment le corpus réuni a pu être mobilisé dans des articles, chapitres d\'ouvrages ou communications autonomes de la thèse, et elle propose des brouillons.</p>
	
	<ul>
		<li class="espacement_vertical">Dans le menu, à droite, le symbole de forme ronde permet de savoir si le panier est plein ou s\'il est vide' . ndbp_a($notes_projets, 1) . ' : rose, il est plein, transparent, il est vide. En cas de doute, il est cliquable, un pop-up s\'ouvre alors pour donner l\'information ; il est alors possible, le cas échéant, de vider le panier. </li>
			<ul>
				<li style="font-size:0.9em;">Les paniers peuvent être composés en cliquant sur les signes «&#8239;plus&#8239;» des vignettes de citation, sur n\'importe quelle page du site.</li>
				<li style="font-size:0.9em;">Lorsqu\'un nouveau projet est créé alors que le panier est plein, il enregistre d\'emblée les citations contenues dans le panier dans ses données.</li>
			</ul>
		<li class="espacement_vertical">La page présente la liste des projets déjà enregistrés&#8239;: les publications, les communications dans le cas où elles n\'ont pas vraiment donné lieu à publication, et les brouillons. Elles sont classées par date' . ndbp_a($notes_projets, 2) . ', en ordre décroissant.</li>
		<li class="espacement_vertical">Ces éléments sont associés à une case à cocher, qui peut renvoyer à deux usages différents&#8239;: soit «&#8239;Chercher&#8239;», et dans ce cas la page affiche toutes les citations qui sont utilisées dans tel ou tel projet, soit «&#8239;Mettre à jour le projet avec le contenu du panier&#8239;», dans ce cas le panier temporaire est stocké définitivement dans Litote, et la page affiche ensuite, pour le projet concerné, les citations anciennes et nouvelles qui lui sont associées.</li>
	</ul></p>
	
	<p class="left">Ce qui concerne publications et communications renvoie à du contenu froid, dont l\'intérêt est plus bibliographique que productif. <strong>Les brouillons</strong>, en revanche, sont extrêmement utiles en cours de rédaction d\'un projet ou d\'un autre&#8239;: au cours de la thèse, notamment, j\'en ai créé et supprimé successivement des dizaines, pour me constituer différentes bases de travail. Je les utilise aussi pour stocker des idées d\'articles pour plus tard.</p>
	
	<p class="left">À noter&#8239;: comme, dans le cadre de cette version de démonstration, j\'ai verrouillé l\'enregistrement de nouvelles données dans la base, vous ne pouvez pas manipuler ces brouillons vous-mêmes, ni enregistrer vos paniers pour les voir apparaître ici sous forme de nouveaux projets. En revanche, j\'ai fait en sorte qu\'un clic sur «&#8239;Mettre à jour le projet avec le contenu du panier&#8239;» vous permette d\'afficher le contenu de votre panier.</p>'
	
	. $impressions
	
	. fin_page($notes_projets);
}	

elseif(isset($_GET['mode-d-emploi']) && $_GET['mode-d-emploi'] == 'modif') {
	
	$notes_modif = array(
	
		1 => array('nom' => 'html', 
			'contenu' => 'Le code final enregistré dans la base de données, et retraduit ensuite pour divers affichages, est en html&#8239;: l\'utilisateurice peut donc coder la citation en html si cela lui chante.'),
		2 => array('nom' => 'markdown',
			'contenu' => 'En réalité, le code de transcription est très bricolé, j\'ai utilisé des rechercher-remplacer en fonction d\'expressions régulières, écrites à la main. Il doit exister des manières de faire plus propres, notamment en passant par du code javascript...'),
		3 => array('nom' => 'guillemets',
			'contenu' => 'Dans l\'idée que les citations seront utilisées à l\'intérieur de texte qui... les citeront. Si elles comprennent des guillemets, ceux-ci deviendront donc, la plupart du temps, des guillemets à l\'intérieur d\'autres guillemets&#8239;: l\'usage typographique en France implique d\'utiliser des guillemets anglais pour ces cas-là.'),
		4 => array('nom' => 'scan',
			'contenu' => 'Grâce aux Archives Lesbiennes du Québec, et en particulier grâce à Louise Turcotte, bénie soit-elle, j\'ai pu utiliser un scanneur à bras, très utile pour numériser rapidement des textes longs, les OCRiser et ainsi les rendre à la fois consultables par ctrl+F, et copiables. Son usage a considérablement accéléré mon travail.')
		
	);
	
	echo '<h2>Mode d\'emploi de la page des modifications de données</h2>
	
	<p class="left">La page des modifications permet d\'ajouter ou de modifier, selon l\'information envoyée par l\'utilisateurice au site, tous types d\informations structurelles pour Litote.</p>
	
	<h3>Ajouter ou modifier une œuvre</h3>
	
	<p class="left">Comme pour les citations, l\'appel à création d\'une nouvelle œuvre se joue dans le menu que l\'on retrouve en haut de chaque page du site.</p>
	
	<p class="left">S\'ouvrent alors plusieurs champs&#8239;:</p>
	<ul>
		<li class="espacement_vertical">Prénom, nom&#8239;: si l\'autrice est seule, son prénom et son nom&#8239;; si elles sont plusieurs, les séparer par des points-virgules, en veillant à respecter l\'ordre de leur apparition.</li>
		<li class="espacement_vertical">Les champs suivants sont assez clairs, je pense&#8239;; comme pour les citations (voir plus bas), le texte doit être écrit en markdown.</li>
	</ul>
	
	
	<br>
	<h3>Ajouter ou modifier une citation</h3>
	
	<p class="left">Comme pour les œuvres, l\'appel à création d\'une nouvelle citation se joue dans le menu que l\'on retrouve en haut de chaque page du site.</p>
	
	<p class="left">S\'ouvrent alors plusieurs champs&#8239;:</p>
	<ul>
		<li class="espacement_vertical">Titre de l\'œuvre&#8239;: par défaut, est pré-sélectionné celui de la dernière citation enregistrée dans la base de données (pratique).</li>
		<li class="espacement_vertical">Citation&#8239;: la citation en elle-même, bien sûr. 
			<ul>
			<li class="espacement_vertical">L\'utilisateurice doit l\'écrire en mardown' . ndbp_a($notes_modif, 1) . '. ' . explications_markdown() . ndbp_a($notes_modif, 2) . ' Les guillemets sont automatiquement transformés en guillemets anglais' . ndbp_a($notes_modif, 3) . ', et les apostrophes droites en apostrophes courbes, pour faciliter l\'harmonisation typographique des documents rédigés.</li>
			<li class="espacement_vertical">Astuce&#8239;: ma base de données contenant actuellement plus de 3000 citations, j\'ai perdu <em>beaucoup</em> de temps, au cours de ma thèse, à les recopier à la main à partir des livres que je devais consulter. Pour aller plus vite, il faut penser à l\'OCR automatique des textes&#8239;: les prendre en photo, les passer à la moulinette OCR (quelle que soit votre outil pour cela, logiciel en ligne ou téléchargé, application de scan...' . ndbp_a($notes_modif, 4) . '), puis les copier et corriger sur cette page.
			</ul>
		<li class="espacement_vertical">Numéro de page&#8239;: simple (exemple&#8239;: 143), ou composé (exemple&#8239;: 143-144). Les citations de l\'œuvre seront ensuite affichées dans l\'ordre des numéros de page.</li>
		<li class="espacement_vertical">Notes&#8239;: champ libre, qui peut préciser quelques informations sur la citation, de contextualisation notamment, ou rester vide.</li>
		<li class="espacement_vertical">Les étiquettes&#8239;: à cocher en fonction de ce à quoi correspond la citation.</li>
		<li class="espacement_vertical">Le plan&#8239;: à cocher en fonction de leur inscription, souhaitée ou avérée, dans tel ou tel chapitre du plan.</li>
		<ul>
			<li class="espacement_vertical">À noter : l\'ergonomie pourrait être améliorée. Actuellement, trois possibilités d\'enregistrement par rapport au plan : 1. la case à cocher permet d\'indiquer si une citation doit être stockée dans tel ou tel chapitre ; 2. la ligne «&#8239;Citations confirmée&#8239;» sert à indiquer, soit par un numéro de page précis en cas d\'ouvrage fini, soit par un texte aléatoire, que la citation est déjà employée dans tel ou tel chapitre ; 3. la ligne «&#8239;Url&#8239;» ne sert que dans le cas d\'une publication numérique du travail fini, pour renvoyer directement aux endroits où la citation est analysée.</li>
		</ul>
		<li class="espacement_vertical">Les projets&#8239;: à cocher en fonction de leur utilisation, souhaitée ou avérée, dans tel ou tel projet ou brouillon.</li>
	</ul>
	
	<p class="left">Si la citation est en cours de modification, ses données seront mises à jour&#8239;; s\'il s\'agit d\'un ajout, elle sera enregistrée dans la base de données.</p>
	
	<br>
	<h3>Ajouter ou modifier un projet</h3>
	
	<p class="left">L\'appel à création de nouveaux projets se fait dans la page des projets&#8239;; si le panier est plein, le projet créé impliquera les citations qui y sont stockées, sinon il sera créé vide.</p>
	
	<p class="left">L\'utilisateurice peut enregistrer titre, date (selon celle qui lui paraît pertinente), type de projet&#8239;; la référence bibliographique doit être rédigée en code markdown.</p>
	
	<br><br>
	
	<p class="left">Dans tous les cas, si l\'utilisateurice tente d\'enregistrer une donnée qui est déjà présente dans la base, le site l\'informera qu\'elle était déjà présente. Sinon, il confirmera la réussite de l\'ajout.</p>' 
	
	. fin_page($notes_modif);
}

elseif(isset($_GET['mode-d-emploi']) && $_GET['mode-d-emploi'] == 'modalites-legales') {
	
	$notes_legal = array(
	
		1 => array('nom' => 'delai', 
			'contenu' => 'Sur ce point je dois préciser qu’il va y avoir un décalage de quelques mois entre le dépôt de la thèse et la mise en ligne de ces annexes, et la mise à disposition des liens hypertextes : en effet je ne peux les mettre en place tant que ma thèse n’est pas officiellement mise en ligne par mon université. Je fais en sorte qu’elle ne soit pas répertoriée par les moteurs de recherche en attendant.')		
	);
	
	echo '<h2>Modalités légales de mise en ligne des citations</h2>
	
	<h3>Contexte</h3>

	<p class="left">Litote est un logiciel de gestion de corpus de textes que j’ai développé pour accompagner ma thèse, à partir de 2018. L’adresse des annexes proposées ici est pérenne, grâce au service d’hébergement mutualisé spécialisé pour la recherche en humanités numériques que propose HumaNum. Le code du logiciel est mis à libre disposition sur son dépôt GitLab, et officiellement rattaché à mes publications en tant que chercheuse grâce à son référencement sur Software Heritage, qui fait le lien avec ma page HAL (il constitue en soi un « texte » publié).</p>

	<p class="left">Dans le cadre d’une thèse, le droit de citation est garanti pour les œuvres officiellement publiées et ne pose pas spécifiquement de problèmes de droits d’auteurs. Dans le cas de ce dédoublement du support de la thèse (papier et numérique), où les citations sont déployées loin de leur analyse (elles y renvoient par numéro de page, mais sur Litote on les lit seules : les dispositifs sont liés, mais distants et de natures différentes), la question des droits d’auteurs paraît plus ambiguë. Il n’existe manifestement pas de précédent en la matière. Le service juridique de Sorbonne Université m’a proposé, en mars-avril 2023, un ensemble de règles qui me permettent de publier ces annexes en conformité avec la législation, sous le « régime de la courte citation tel que prévue par l’article L. 122-5 du code de la propriété intellectuelle ». En voici les conditions, je reproduis leurs indications : </p>
	
	<ul>
		<li class="espacement_vertical"><strong>A. Dispositif Litote.</strong> Spécificités du dispositif en l’occurrence : le projet d’annexes sur Litote, en ligne, implique de regrouper ces citations au sein d’un support distinct de la thèse elle-même ; la problématique est ici de déterminer si le fait que l’annexe ne soit pas incorporée physiquement à la thèse, et le fait qu’elle soit accessible en open source, la fait _de facto_ sortir du champs du régime de la courte citation. De fait, la base de données ainsi constituée ne saurait toutefois être regardée comme une œuvre à caractère scientifique à part entière, même si elle implique une interface graphique organisée et triée : cette base de donnée ne pourrait en effet exister sans les citations qu’elle contient.</li>
			<ul>
			<li >Dans la mesure où cette annexe consiste en une interface graphique de sélection et de mise en page des citations, elle intègre cependant, pour chaque citation, des liens renvoyant vers les chapitres de thèse qui s’y réfèrent : dès lors, il semble possible de considérer que cette base de données s’intègre bien à la thèse qu’elle entend illustrer. Dans ces conditions, sous réserve que les autres exigences liées à l’exception de courte citation soient réunies, on peut considérer que le fait qu’elle soit accessible en open source n’a pas d’incidence.</li>
			</ul>
		
		<li class="espacement_vertical"><strong>B. Respect du régime de la courte citation tel que prévue par l’article L. 122-5 du code de la propriété intellectuelle.</strong></li>
		
		<ul>
			<li class="espacement_vertical">B. 1. Le droit de citation est garanti sans besoin de demander l’autorisation préalable de l’auteur ou de l’autrice dès lors que son nom et la source sont <strong>clairement référencés, et qu’il est « justifié par le caractère critique, polémique, pédagogique, scientifique ou d’information</strong> de l’œuvre à laquelle elles sont incorporées » – ici c’est le caractère scientifique qui compte. Chaque vignette de citation présente en effet le nom de l’autrice concernée ainsi que le référencement exact de la source, incluant sa pagination précise, et l’endroit de la version définitive du fichier de thèse où cette citation est analysée.</li>
			<li class="espacement_vertical">B. 2. Les extraits cités doivent être <strong>courts relativement à l’œuvre originale</strong>. Ce critère de brièveté s’apprécie par rapport à la longueur de l’œuvre dans laquelle la citation est insérée, mais aussi par rapport à l’œuvre dont la citation est extraite. Cette appréciation est délivrée au cas par cas en jurisprudence : il n’existe pas d’échelle précise à partir de laquelle la citation peut être considérée comme courte. J’ai veillé à ce que les citations de chaque vignette soit en effet courtes ; elles correspondent ni plus ni moins à l’analyse qui en est proposée dans le corps de la thèse (pour déterminer début et fin). J’ai veillé aussi à ce que leur nombre ne soit pas trop grand, en proportion au volume de chacune des œuvres étudiées, afin d’éviter un effet « morceaux choisis » qui donnerait trop l’impression de donner à lire l’ensemble de l’œuvre.</li>
			<li class="espacement_vertical">B. 3. Ils doivent <strong>s’intégrer dans une œuvre personnelle</strong> suffisante pour justifier la citation, référencer et délimiter les citations. La citation doit ainsi permettre d’illustrer en principe une discussion ou une argumentation formant la matière principale d’une œuvre seconde. Ce critère est garanti par le renvoi systématique et précis aux analyses de texte proposées dans la thèse ; ceux-ci sont à la fois de type sémantique (renvoi à la pagination), et de type hypertexte (liens renvoyant directement au passage' . ndbp_a($notes_legal, 1) . '). Il y a deux cas de figure dans ces annexes :</li>
			<ul>
				<li>Premier cas de figure, majoritaire, citation disponible sur Litote parce qu’elle est citée et analysée dans le texte de la thèse : cas simple qui correspond parfaitement à la description ci-dessus.</li>
				<li>Second cas de figure, citation disponible sur Litote en tant qu’elle fait partie d’un « échantillon » d’extraits illustrant un argument donné dans la thèse, où l’argument s’appuie sur la récurrence et certains traits d’écriture. Dans ce cas, la citation en elle-même n’est pas précisément analysée dans le texte de la thèse elle illustre et étaye, en tant qu’elle est en lien avec d’autres citations, un point mentionné, en complétant les analyses de citations proposées dans le corps de la thèse. Dans ce cas, j’ai fait en sorte que la citation soit la plus courte possible, afin de vraiment m’assurer qu’elle ne contienne rien d’autre que ce qui correspond précisément à l’argument d’appel (sauf dans le cas où elle est aussi utilisée ailleurs et expliquée plus longuement, auquel cas cela est indiqué au bas de chaque vignette, « Aussi utilisée pour » et renvoi vers la page de thèse en question.)</li>
			</ul>
			
			<li class="espacement_vertical">B. 4. <strong>Ils ne doivent pas altérer le sens du texte d’origine (droit moral)</strong> ; il est également recommandé d’isoler la citation de manière visible pour le lecteur, en utilisant des guillemets par exemple. Ce critère n’encourt pas de risque : les citations sont livrées de manière exacte dans Litote, sans déformation par le dispositif discursif qui l’environne. Les vignettes de définition sont clairement délimitées par leur encadrement.</li>
		</ul>	
	</ul>'
	
	. fin_page($notes_legal);
}

else echo '<p class="left">Je ne sais pas de quelle rubrique vous souhaitez obtenir le mode d\'emploi.</p>' . fin_page('');
	
	
	
function ndbp_a($groupe, $numero) {
	return '<sup style="font-size:0.8em;"><a title="' . $groupe[$numero]['contenu'] . '" 
			href="#n' . $numero . '" id="a' . $numero . '">' . $numero . '</a></sup>';
	
}

function ndbp_n($groupe) {
	$a = '';
	foreach($groupe as $num => $array) {
		$a .= '<a href="#a' . $num . '" id="n' . $num . '">' . $num . '.</a> ' . $groupe[$num]['contenu'] . '<br><br>';
	}
	return $a;
}

function fin_page($notes) {
	$a = '<p class="left"><br><br>Vous pouvez fermer cette fenêtre, revenir à la <a href="' . var_index_demo() . '">page d\'accueil principale</a>.</p>
	
	<p class="left">Crédits : site administré par Aurore Turbiau. Contact : <a href="mailto:aurore.turbiau@zaclys.com">aurore.turbiau [at] zaclys.com</a>. Voir les <a href="a-propos.php?mode-d-emploi=modalites-legales">modalités légales de mise en ligne des citations.</a></p>';
	
	if($notes != null) {
		
	$a .= '<div class="ndbp" style="margin-top:40px;margin-left:20px;">
		
		<strong>Notes&#8239;:</strong><br>' 
			
			. ndbp_n($notes) . '
			
		</div>';
	}
	
	return $a;
}

?>

</div>

</body>

</html>
