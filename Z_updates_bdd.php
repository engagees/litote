<?php

// S'IL FAUT SUPPRIMER UN ÉLÉMENT *******************************************************************

$types = array(
	array('type' => 'citation', 'table' => 'citations', 'id' => 'ID_citation'),
	array('type' => 'tag', 'table' => 'tags', 'id' => 'ID_tag'),
	array('type' => 'classement', 'table' => 'classementtags', 'id' => 'ID_classement'),
	array('type' => 'projet', 'table' => 'projets', 'id' => 'ID_projet'),
	array('type' => 'oeuvre', 'table' => 'oeuvres', 'id' => 'ID_oeuvre'),
	array('type' => 'chapitre', 'table' => 'chapitres', 'id' => 'ID_chapitre'),
	array('type' => 'partie', 'table' => 'chapitres', 'id' => 'ID_chapitre'));

for($i = 0; $i < count($types); ++$i)		
{
	if(isset($_POST['suppr_' . $types[$i]['type']])) {
	
		// Gestion des structures inter-tables **********************
		
		if($types[$i]['type'] == 'oeuvre' 
			|| $types[$i]['type'] == 'projet'
			|| $types[$i]['type'] == 'classement'
			|| $types[$i]['type'] == 'chapitre') {
			var_updates_suppr($types[$i]['type'], '');
		}
				
		elseif($types[$i]['type'] == 'tag') {			
			$reponse2 = $bdd->query('SELECT tag_bdd FROM tags WHERE ID_tag = ' . $_POST['ID_tag']);
			
			while($don = $reponse2->fetch())
			{	var_updates_suppr('tags', $don);
			}
			$reponse2->closeCursor();			
		}
		
		// Suppression de l'item dans la table principale ***********
		var_updates_suppr_item($types, $i);	
	}
} 

// S'il faut mettre à jour un projet *******************************************************************

if(isset($_POST['update_projet']) && !empty($_SESSION['panier']) && isset($_POST['ID_projet']))  {
	
	foreach($_SESSION['panier'] as $id_auto => $id_cit)
	{
		if($_POST['ID_annexe'] != 'null') $id = $_POST['ID_annexe'];
		elseif($_POST['ID_publication'] != 'null') $id = $_POST['ID_publication'];
		elseif(isset($_POST['ID_brouillon'])) $id = $_POST['ID_brouillon'];

		echo verif_mdp(var_mdp());
		var_verif_mdp();
		if(isset($_SESSION['mdp']) && $_SESSION['mdp'] == var_mdp()) var_updates_panier_projet($id, $id_cit);
	}	
	unset($_SESSION['panier']);
}

// S'il faut mettre à jour un chapitre *******************************************************************

if(isset($_POST['update_chapitre']) && !empty($_SESSION['panier']) && isset($_POST['ID_chapitre']))  {
	
	if($_POST['mode-tri-chap'] == 'utilise') $condition = 'chap' . $_POST['ID_chapitre'] . '_ver="utilisée"';
	else $condition = 'chap' . $_POST['ID_chapitre'] . '=1';
	
	foreach($_SESSION['panier'] as $id_auto => $id_cit)
	{		
		var_updates_panier_chapitre_ou_tags($id_cit, $condition);
	}	
	unset($_SESSION['panier']);
}

// S'il faut mettre à jour des tags avec le panier *******************************************************************

if(isset($_POST['update_tags_panier']) && !empty($_SESSION['panier']))  {
	
	$liste = array();
	$reponse = $bdd->query('SELECT tag_bdd FROM tags');
	while($donnees = $reponse->fetch(PDO::FETCH_ASSOC))
	{
		if(isset($_POST[$donnees['tag_bdd']])) $liste[] = $donnees['tag_bdd'];
	}
	$reponse->closeCursor();
	
	if(!empty($liste)) {	
		$condition = '';
		$compteur = 0;
		foreach($liste as $key => $tag) {
			$virgule = $compteur == 0 ? '' : ', ';
			$condition .= $virgule . $tag . ' =1';
			$compteur++;
		}
		
		echo $condition;
	
		foreach($_SESSION['panier'] as $id_auto => $id_cit)
		{	
			var_updates_panier_chapitre_ou_tags($id_cit, $condition);
		}	
		unset($_SESSION['panier']);
	}
}

//~ FONCTIONS DE BASE DE DONNÉES *******************************************************************

function update_insert($post)
{
	global $bdd;
	
	// MISE EN PLACE DES VARIABLES
	
	// Mise en place des variables s'il s'agit d'un projet à ajouter/modifier **********************
	if(isset($post['ID_projet']) && !isset($post['ID_citation']) && !isset($post['ID_oeuvre']))
	{
		$id = 'ID_projet';
		$table = 'projets';
		$dernier = 'date';
		$verif = 'titre';
		$type = 'projet';
	} 
	
	// Mise en place des variables s'il s'agit d'une œuvre à ajouter/modifier **********************
	if(isset($post['ID_oeuvre']) && !isset($post['ID_citation']) && !isset($post['ID_projet']))
	{
		$id = 'ID_oeuvre';
		$table = 'oeuvres';
		$dernier = 'reflexions';
		$verif = 'titre';
		$type = 'oeuvre';
		
		$post = var_updates_spec($post);
	} 
	
	// Mise en place des variables s'il s'agit d'une citation à ajouter/modifier *******************
	if(isset($post['ID_citation']))
	{
		$id = 'ID_citation';
		$table = 'citations';
		$dernier = 'page';
		$type = $verif = 'citation';
		
		$post = checkbox_vides('tags', $post);	
		$post = checkbox_vides('chapitres', $post);
		$post = checkbox_vides('projets', $post);
	}
	
	// Récup donnée titre **************************************************************************
	
	if(isset($_POST['ID_oeuvre']) && $_POST['ID_oeuvre'] != null) {
		$reponse = $bdd->prepare('SELECT titre FROM oeuvres WHERE ID_oeuvre = ?');
		$reponse->execute(array($post['ID_oeuvre']));
		$temp = $reponse->fetch();
		$titre = $temp['titre'];
		$reponse->closeCursor();
	}
	else $titre = '';
	//~ else $titre = $post['titre'];
	
	// Mise en place des variables s'il s'agit d'un tag à ajouter/modifier **********************
	
	if(isset($post['ID_tag']) || isset($_POST['nouveau_tag']))
	{		
		$id = 'ID_tag';
		$table = 'tags';
		$dernier = 'tag_complet';
		$verif = 'tag_bdd';
		$type = 'tag';
	} 
	
	// Mise en place des variables s'il s'agit d'un classement de tag à ajouter/modifier **********************
	
	if(isset($_POST['nouveau_classement']) || (isset($_POST['ID_classement']) && !isset($_POST['nouveau_tag']) && !isset($_POST['ID_tag'])))
	{
		$id = 'ID_classement';
		$table = 'classementtags';
		$dernier = 'titre';
		$verif = 'ordre';
		$type = 'classement';
	} 
	
	// Mise en place des variables s'il s'agit d'un chapitre à ajouter/modifier **********************
	
	if(isset($post['ID_chapitre']) || isset($_POST['nouveau_chapitre']))
	{
		$id = 'ID_chapitre';
		$table = 'chapitres';
		$dernier = 'titre_court';
		$verif = 'titre';
		$type = 'chapitre';
	} 
	
	// Mise en place des variables s'il s'agit d'une partie à ajouter/modifier **********************
	
	if(isset($post['ID_partie']) || isset($_POST['nouvelle_partie']))
	{
		$id = 'ID_chapitre';
		$table = 'chapitres';
		$dernier = 'titre_court';
		$verif = 'titre';
		$type = 'partie';
	} 
	
	
	// ÉCRITURE DES REQUÊTES SQL
	
	// S'il s'agit d'un ajout **********************************************************************
	
	if(isset($post['ajout_reussi']))
	{
		// Vérifier qu'il ne s'agit pas d'un doublon ***********************************************
		$rep = $bdd->prepare('SELECT ' . $id . ' FROM ' . $table . ' WHERE ' . $verif . '=?');
		$rep->execute(array($post[$verif]));
		if($don = $rep->fetch())
		{
			$doublon = 'oui';			
			echo var_updates_demo('doublon', $type, $don, $id);
		}
		$rep->closeCursor();		
	}
	
	if(isset($post['ajout_reussi'])) {
		
		// Préparation de la requête d'insertion ****************************************************		
		$requete1 = 'INSERT INTO ' . $table . ' SET ';
		$requete3 = $dernier . '=' . secu($post[$dernier], 'quote') . ';';
		$repo = $bdd->query('SELECT * FROM ' . $table);
	}
		
	// S'il s'agit d'une modification ***************************************************************
	
	if(isset($post['modif_reussie']))
	{
		// Préparation de la requête de modification ************************************************
		$requete1 = 'UPDATE ' . $table . ' SET ';		
		$requete3 = $dernier . '=' . secu($post[$dernier], 'quote') . ' WHERE ' . $id . '= ' . secu($post[$id], 'quote') . ';';
		$repo = $bdd->prepare('SELECT * FROM ' . $table . ' WHERE ' . $id . ' = ?');
		$repo->execute(array($post[$id]));
	}
	
	// Vérifier qu'il n'y a pas d'entourloupe *******************************************************
	if($donnees = $repo->fetch())
	{
		$keys = array_filter(array_keys($donnees),'is_string');
	}	
	else return 'Une erreur s\'est produite.';
	$repo->closeCursor();
	
	// Paramétrer les données **************************************
	
	$requete2 = '';
	foreach($keys as $key)
	{
		if(isset($post[$key]) && $key != $dernier && $key != $id)
		{
			// Si le choix du corpus secondaire/noyau/etc. doit être calculé automatiquement plutôt que choisi à la main
			if($key == 'corpus' && $post['corpus'] == 9) {
				$requete2 .= $key . '=' . var_corpus($post) . ', ';
			}
			// Pour la plupart des cas
			else {
				$requete2 .= $key . '=' . secu($post[$key], 'quote') . ',';
			}		
		}		
	}

	// S'IL N'Y A PAS DE DOUBLON, LANCER LES UPDATE OU INSERT ********
	
	if(!isset($doublon)) 
	{
		// Lancement de la requête d'insertion ou de modification ***************************************
		$requete = $requete1 . $requete2 . $requete3;
		//~ echo $requete;
		if(!isset($doublon)) {
			var_updates_requete($requete);
		}
		
		// EN CAS D'AJOUTS OU MODIFS MULTI-TABLES : ****************************************************************
	
		// Pour les projets ***********************************************

		if(isset($post['ID_projet']) && !isset($post['modif_reussie'])) {
		
			$trouverid = $bdd->query('SELECT ID_projet FROM projets WHERE titre=' . secu($_POST[$verif], 'quote'));
			if($temp = $trouverid->fetch())
			{
				$id_projet = $temp['ID_projet'];
			}
		
			// 1. compléter par la création des colonnes correspondantes dans la table citations ********
		
			var_updates_ajout('projet', $id_projet);
	
			// Et 2. enregistrer le panier dans le projet s'il y a un panier ****************************
	
			if(isset($_SESSION['panier'])) {
				foreach($_SESSION['panier'] as $id_auto => $id_cit)
				{
				var_updates_panier_projet($id_projet, $id_cit);
				}
			}
		}

		// Pour les tags **************************************************
		
		if($id == 'ID_tag' && isset($post['ajout_reussi'])) {
			var_updates_ajout('tag', $post[$verif]);	
		}
		
		//~ elseif($id == 'ID_tag' && isset($post['modif_reussie'])) {
			//~ var_rename_tag($post, $verif);							
		//~ }
		
		// Pour les chapitres *********************************************
		
		if(isset($post['nouveau_chapitre']) && isset($post['ajout_reussi'])) {
			
			$check_id = $bdd->query('SELECT ID_chapitre FROM chapitres WHERE ' . $verif . '=' . secu($post[$verif], 'quote'));
			
			while($cle = $check_id->fetch(PDO::FETCH_ASSOC))
			{
			var_create_chap($cle);	
			}
			$check_id->closeCursor();	
		}	
	
	
		// CONFIRMATION RÉUSSITE ******************************************************************
	
		echo '<div class="modif_citation">';
	
		if(!isset($_GET['modif_oeuvre']))
		{		
			if(isset($_POST['ajout_reussi']))
			{
				$conf = $bdd->query('SELECT ' . $id . ' FROM ' . $table . ' ORDER BY ' . $id . ' DESC LIMIT 1');
				if($d = $conf->fetch())
				{
					$value = $d[$id];
				}
				echo var_updates_demo('ajout', '', '', '');
			}
			if(isset($_POST['modif_reussie']))
			{
				echo var_updates_demo('modif', '', '', '');
				$value = secu($post[$id], 'quote');
			}
			
			if($type == 'partie') $type2 = 'chapitre';
			else $type2 = $type;
						
			echo '<form method="get" action="modif.php">
				<input type="hidden" value=' . $value . ' name="modif_' . $type . '"/>
				<input type="hidden" value=' . $value . ' name="ID_' . $type2 . '"/>
				<input type="hidden" name="type_config" value="' . $type . '">
				<input type="submit" class="bouton_final" value="Revoir les données enregistrées" />
			</form>';
			
			if(isset($_POST['ID_oeuvre'])) {
			
				echo '<form method="post" action="' . var_index_demo() . '">
				<input type="hidden" value="' . $titre . '" name="titre"/>
				<input type="submit" class="bouton_final" value="Voir les autres citations de l\'œuvre" />
			</form>';
			}
		}
		
		echo '</div>';
	}
}

// Fonction appelée pour la gestion des cases à cocher restées vides

function checkbox_vides($table, $post) {
	
	global $bdd;
	
	$where = $table == 'chapitres' ? ' WHERE chap!="partie"' : '';
	
	$reponse = $bdd->query('SELECT * FROM ' . $table . $where);
	while($donnees = $reponse->fetch(PDO::FETCH_ASSOC)) {
		
		if($table == 'tags') $repere = $donnees['tag_bdd'];
		elseif($table == 'projets') $repere = 'projet' . $donnees['ID_projet']; 
		
		elseif($table == 'chapitres') {
			$repere = 'chap' . $donnees['ID_chapitre']; 
			$repere_ver = 'chap' . $donnees['ID_chapitre'] . '_ver'; 
		}
				
		$post[$repere] = isset($post[$repere]) ? 1 : 0;
		if(isset($repere_ver)) {
			$post[$repere_ver] = isset($post[$repere_ver]) ? 1 : 0;
		}
	}
	$reponse->closeCursor();
	
	return $post;
}

?>
