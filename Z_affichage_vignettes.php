<?php

if(isset($_POST['annexe']) && $_POST['annexe'] != 'null') {
	$_POST['ID_projet'] = $_POST['annexe']; 
}

elseif(isset($_POST['publication']) && $_POST['publication'] != 'null') $_POST['ID_projet'] = $_POST['publication'];

if(isset($_POST['titre']) || isset($_POST['textearechercher']) || isset($_POST['ID_projet'])) {

	// RÉCUPÉRATION DES CITATIONS EN FONCTION DES CRITÈRES CHOISIS OU DE LA RECHERCHE***************
	
	// 1. Création d'une variable citation *********************************************************

	$citations = tri_citations($bdd);
	
	// 2. Mise en ordre de l'affichage : date croissante, livre par livre, page croissante *********
	
	if($citations != false) {
		usort($citations,
			function($a,$b){
				if ($a['origine_date'] < $b['origine_date'])
					return -1;
				elseif ($a['origine_date'] > $b['origine_date'])
					return 1;
				else {				
					if ($a['titre'] < $b['titre'])
						return -1;
					elseif ($a['titre'] > $b['titre'])
						return 1;
					else {
						if ($a['page'] == $b['page'])
							return 0;
						elseif ((int) $a['page'] < (int) $b['page'])
							return -1;
						else
							return 1;
					}
				}
			});
	}	
		
	// 3. Affichage final des citations sélectionnées **********************************************
	
	$info_bandeau = infobulle('Cette ligne indique combien de citations correspondent aux critères de recherche proposés. Dessous, sont listées l\'ensemble des œuvres pour lesquelles la recherche a trouvé des citations pertinentes : chaque ligne correspond à un lien qui renvoie directement aux citations de cette œuvre précise dans la page.');
	
	echo '<div class="bandeau_resultats">
	
	<div class="left">';
	
	if($citations == null)
		echo 'Aucune citation ne correspond à cette recherche pour le moment. Vous n\'avez peut-être pas formulé de recherche suffisamment précise.</div>';
	
	else 
	{
		if(count($citations) == 1)
			echo 'Une citation correspond à la recherche.';
		else 
			echo count($citations) . ' citations correspondent à la recherche.';
			
		echo $info_bandeau . var_info_pillage();
	}
	
	echo '</div>';
	
	if($citations != null) {
		
		echo '<br><div class="navig_citations" style="text-align:left;"><ul>';
		
		$liste = array();
		foreach($citations as $array)
		{
			$a = '<li><a href="#' . $array['ID_oeuvre'] . '" style="color:black;">
				<em>' . $array['titre'] . '</em> (' . $array['origine_date'] . ')
				</a></li>';
			
			if(!in_array($a, $liste))
			{
				$liste[] = $a;
				echo $a;
			}			
		}
	
		echo '</ul></div></div>';
				
		echo bloc_citation($citations);
	}
}

else
{
	// RÉCUPÉRATION DES CITATIONS SI AUCUN CRITÈRE DE SÉLECTION : AFFICHAGE PAR DÉFAUT **************

	$citations = array();
	
	// 1. Sélection de deux citations pour deux livres au hasard ************************************

	$rep = $bdd->query('SELECT DISTINCT titre, ID_citation FROM oeuvres o LEFT JOIN citations c
	ON o.ID_oeuvre = c.ID_oeuvre
	ORDER BY RAND() LIMIT 2');
	while($donnees = $rep->fetch())
	{	
		$reponse = $bdd->prepare('SELECT * FROM citations c LEFT JOIN oeuvres o
		ON c.ID_oeuvre = o.ID_oeuvre
		WHERE titre = ? 
		ORDER BY RAND() LIMIT 1');
		$reponse->execute(array($donnees['titre']));
		
		while($donnees = $reponse->fetch(PDO::FETCH_ASSOC))
		{
			$citations[] = $donnees;
		}
		$reponse->closeCursor();
	}
	$rep->closeCursor();
	
	// 2. Affichage des deux citations prises au hasard *********************************************
	
	echo bloc_citation($citations);
}

// 4. Affichage des citations vignette par vignette********************************

function bloc_citation($citations)
{
	global $bdd;
	
	$a = '<div style="position:absolute;display:inline-block;text-align:right;margin-left:-10px;">' . 
		infobulle('À partir de là, s\'affichent soit, en l\'absence de recherche effectuée, deux citations aléatoirement choisies dans la base de données, soit les différentes citations pêchées dans la base en fonction des critères de recherche donnés. Chacune reprend les données éditoriales de son identification – maison d\'édition, date, pagination –, indique d\'éventuelles notes et les étiquettes auxquelles la citation est rattachée, ainsi que l\'usage qui a été fait ou non de cette citation au cours de la thèse. Le symbole “plus”, en haut à gauche, permet d\'ajouter la citation à un panier virtuel, utile pour composer des lots de citations correspondant à des brouillons ou à des publications spécifiques, par exemple.') . '</div>';
	
	foreach($citations as $array)
	{
		// Voir s'il y a plusieurs autrices pour un livre ou non **************************************
		$autrice = check_multi_auteurs($array);
		
		// Prévoir d'afficher différemment les citations si elles sont ou non dans le corpus primaire 
		$couleur = var_affich_couleurs($array);
		
		$a .= '<div id="' . $array['ID_oeuvre'] . '" class="infos_livre ' . $couleur . '">

		<div class="titres '. $couleur . '">';
		
		// Affichage du premier bandeau de couleur : titre, autrice·s + lien vers la page de résumé***
		$a .= '<a target="_blank" href="oeuvres.php?oeuvre=' . $array['ID_oeuvre'] . '">' . $array['ID_oeuvre'] . ' - ' . $array['titre'] . ', ' . $autrice . '</a>
		</div>';
		
		// Affichage du bouton d'ajout au panier *****************************************************
		$a .= '<div class="bouton_panier">
				<a onclick="openPanier(' . $array['ID_citation'] . ');" title="Ajouter la citation au panier">+</a> 
			</div>';
		
		// Affichage du bouton de modification de la citation ****************************************
		$a .= '<div class="rehausser">
		<div class="gris origine">
		<form method="get" action="modif.php">
			<input type="hidden" name="modif_citation" value="' . $array['ID_citation'] . '">
			<input type="submit" value="Modifier">
		</form>';
		
		// Affichage des infos et de la citation, div par div ****************************************

		// Données éditoriales
		
		$a .= $array['origine_date'] . ', 
		éditions ' . $array['origine_edition'] . ', ' 
		. $array['origine_lieu'] . '.
		</div>
		
		<div class="gris courant">
		<p><strong>Édition utilisée</strong> : ' 
		. $array['courant_date'] . ', 
		éditions ' . $array['courant_edition'] . ', ' 
		. $array['courant_lieu'] . '.</p>
		</div>';
		

		// Citation
		
		$a .= '<div class="citation2">
		' . $array['citation'] . ' (p.&#8239;' . $array['page'] . ')
		</div>';
		

		// Contexte et notes
		
		if($array['contexte'] != '')
		{
			$a .= '<div class="gris notes">
			<p><strong>Notes : </strong>' . $array['contexte'] . '</p>
			</div>';
		}
		
		// Citée dans
		
		$citee_dans_in = '<div class="ref_these">
			<p><strong>Citée dans&#8239;: </strong>';
			
		$each_chap1 = si_utilisee($array, '_ver')[0];
		
		if($each_chap1 != '') {
			$a .= $citee_dans_in . $each_chap1 . '</p></div>';
		} 
		
		// Proposée dans
		
		$proposee_dans_in = '<div class="gris tags2">
			<p><strong>Proposée pour&#8239;: </strong>';
		
		$each_chap2 = si_utilisee($array, '')[1];
		
		if($each_chap2 != '') {
			$a .= $proposee_dans_in . $each_chap2 . '</p></div>';
		} 
		
		// Tagguée dans
		
		$a .= '<div class="gris tags2">
			<p><strong>Tags : </strong>';
		
			$reponse = $bdd->query('SELECT tag_bdd, tag_complet FROM tags ORDER BY tag_complet');

			while($donnees = $reponse->fetch(PDO::FETCH_ASSOC))
			{
				$a .= $array[$donnees['tag_bdd']] == 1 ? $donnees['tag_complet'] . '&#8239;; ' : '';
			}
			$reponse->closeCursor();
		
	$a .= '</div>
	</div>
	</div>';

	}
	
	return $a;
}

?>
