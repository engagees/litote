<?php

// FICHIER PERMETTANT, ACTUELLEMENT, DE PRENDRE EN CHARGE LES DIFFÉRENCES DE VERSIONS DE LITOTE, SELON LES PROJETS 


// PAGES D'INITIALISATIONS ***********************************************************************

// 1. Appelées dans le fichier de connexion : ce sont les informations de lien entre le code, la bdd et votre serveur local ou en ligne. Commentez / décommentez selon celle dont vous avez besoin et indiquez vos propres informations.

//~ // En ligne
//~ function var_connexion_serveur() { return 'serveur'; }
//~ function var_connexion_identifiant() { return 'identifiant'; }
//~ function var_connexion_dbname() { return 'nom de la base de données'; }
//~ function var_connexion_mdp() { return 'MDP'; }

// En local
function var_connexion_serveur() { return 'localhost'; }
function var_connexion_identifiant() { return 'identifiant'; }
function var_connexion_dbname() { return 'nom de la base de données'; }
function var_connexion_mdp() { return 'MDP'; }

// Mode de connexion (à changer potentiellement si certificats particuliers, mais la plupart du temps ça marchera avec ça)
function var_methode_connexion($serveur, $dbname, $identifiant, $mdp) {
	// Sans besoin de certificat, cas normal
	try{$bdd = new PDO("mysql:host=$serveur; dbname=$dbname; charset=utf8", $identifiant, $mdp);}
	catch(Exception $e){
		die('Erreur : ' . $e->getMessage());
	}
	return $bdd;
		
	//~ // En cas de certificat
	//~ try{$bdd = new PDO("mysql:host=$serveur; dbname=$dbname; charset=utf8", $identifiant, $mdp,array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8", PDO::MYSQL_ATTR_SSL_VERIFY_SERVER_CERT => "false",PDO::MYSQL_ATTR_SSL_CA => "lien"));}
	
	//~ catch(Exception $e){
		//~ die('Erreur : ' . $e->getMessage());
	//~ }
	//~ return $bdd;
}

// 2. Appelées dans le fichier de connexion : mot de passe d'utilisation de Litote, pour pouvoir accéder aux pages de modification, etc. Choisissez ce que vous voulez.

function var_mdp() {
	return 'MDP';
}


// À ÉVENTUELLEMENT CHANGER POUR ADAPTER LITOTE À DIFFÉRENTS PROJETS ***********************************************************************

// 1. PARAMÉTRER LES PRINCIPAUX FILTRES DE RECHERCHE À CHOIX MULTIPLES

// 1. a. Choix des critères. Par défaut il y en a deux : auteurices et titres ; plusieurs autres possibilités déjà intégrées à activer ou désactiver. Deux solutions : 

	// - Soit utiliser le déjà existant, quitte à changer les labels (par exemple, si vous voulez utiliser une variable "taille de l'auteurice" (why not) - ça n'est pas prévu, mais si vous n'utilisez pas la colonne "periode" déjà prévue dans la table, pourquoi pas l'utiliser à la place. Dans ce cas, coller une ligne "$a .= criteres_choix_multiples_simple('periode', 'Taille');" (vous utilisez un repère bdd préexistant et en changez juste le label). Cette solution n'est pas très propre mais elle est facile et efficace.
	
	// - Soit intervenir directement dans la base de données pour créer une nouvelle colonne dans la table "œuvres" qui corresponde à votre besoin, et ici indiquer la ligne de code permettant de la traiter.

function var_critere_spec() {

	// Les premières lignes restent en place
	global $bdd;
	
	$infobulle_editoriales = '<div style="margin-top:-20px;text-align:right;margin-right:-5px;">' 
		. infobulle('Recherche par données éditoriales : les critères se combinent sur le mode logique du « et », de manière à restreindre au maximum le champ de la recherche (choix d\'' . var_auteurice() . ' + choix d\'un genre, par exemple).') . '</div>';
		
	// Ici vous pouvez supprimer ou ajouter des lignes. Attention à laisser la première en "$a =" (début de la chaîne de caractères) et à entamer les autres avec "$a .=" (concaténation des chaînes).
	
	// Pays
	$a = criteres_choix_multiples_simple('nationalite', 'Pays');
	
	// Genre littéraire	
	$a .= criteres_choix_multiples_simple('genre_litt', 'Genre littéraire');
		
	// Maisons d'édition
	$a .= criteres_choix_multiples_simple('origine_edition', 'Édition');
		
	//~ Possibilités d'ajout déjà prévues dans la table œuvres, en mode texte libre de 50 caractères max : 
	//~ - Pays (activé) (bdd : nationalite)
	//~ - Genre littéraire (activé) (bdd : genre_litt)
	//~ - Maison d'édition (activé) (bdd : origine_edition)
	//~ - Type (bdd : type)
	//~ - Point de vue narratif (bdd : pdv)
	//~ - Genre / sexe (bdd : genre_sexe)
	//~ - Sexualité (bdd : sexualite)
	//~ - Période (bdd : periode)
	
	// Les dernières lignes restent en place
	$a .= $infobulle_editoriales . '
	</div>';
			
	return $a;
}


// 1. b. Affichage de la catégorie "auteur" (appelée dans Z_criteres_recherche.php) : remplacer "auteur·ice" par "auteur", "auteurice", "autrice", selon besoins et envies
function var_auteurice() { return 'auteur·ice'; }



// 1. c. Au moment d'enregistrer les œuvres, appelée dans le fichier de modification des données (modif.php)
function var_champs_modif_inputs($donnees) {
	return '';
	
	// Pour ajouter des champs, par exemple dans le modèle Carla Robison
	//~ return m_input($donnees, 'genre_sexe', 'Genre/sexe')
		//~ . m_select($donnees, 'pdv', 'Point de vue')	. '<br>'
		//~ . m_input($donnees, 'periode', 'Période');
}


// *****************************************************

// 2. PARAMÉTRER LES PRINCIPAUX FILTRES DE RECHERCHE À CHOIX SIMPLES (oui/non, par défaut vides)

// 2. a. Au moment d'enregistrer les œuvres

// Appelée dans le fichier de modification des données (modif.php)
function var_champs_modif_booleens($donnees) {
	return '';
	
	//~ // Pour ajouter des checkbox, par exemple dans le modèle Carla Robison
	//~ return m_checkbox($donnees, 'cinema', 'Adaptation cinématographique') . '<br>'		
			//~ . m_checkbox($donnees, 'autobiographie', 'Autobiographie'). '<br>';
}


function var_criteres_boolens() {
	return '';
	// À coder si besoin de booléens qui regroupent différentes possibilités des critères multiples, en faisant des checkbox : par exemple "<input type="checkbox" value="ident_genre" name="ident_genre"' . $sexise . '/>Auteur·ice sexisé·e<br>" pour regrouper tous les cas où l'auteur·ice n'est pas un homme.
}

// 2. c. Au moment de trier les citations (Z_recherche_citations.php

function var_condition_booleens($conditions) {
	
	return $conditions;
	// À coder si besoin de booléens qui regroupent différentes possibilités des critères multiples, en faisant des checkbox : par exemple "if(isset($_POST['ident_genre'])) $conditions['genre_sexe'] = 'genre_sexe != "Homme"';" pour regrouper tous les cas où l'auteur·ice n'est pas un homme.
}


// *****************************************************

// 3. UTILISATION DES FILTRES DE RECHERCHE SPÉCIFIQUES DANS LA PAGE DES ŒUVRES

// 3. a. Affichage
function var_champs_oeuvres_spec($donnees) { 
	return ''; // Par défaut vide
	
	//~ // Par exemple dans le cas du travail de Carla Robison
	//~ $booleens = array();
	//~ $array = array('autobiographie', 'cinema', 'engagement');
	//~ foreach($array as &$key) {
		//~ $booleens[$key] = $donnees[$key] == 1 ? 'oui' : 'non';
	//~ }
	
	//~ return '<div class="origine">
				//~ <p><strong>Point de vue</strong> : ' . $donnees['pdv'] . '<br><br>
				//~ Genre / sexe: ' . $donnees['genre_sexe'] . '<br>
				//~ Dimension autobiographique : ' . $booleens['autobiographie'] . '<br></p>
			//~ </div>

			//~ <div class="origine">
				//~ <p>Pays de publication : ' . $donnees['pays'] . '<br>
				//~ Genre littéraire : ' . $donnees['genre'] . '<br>
				//~ Adaptation cinématographique : ' . $booleens['cinema'] . '<br></p>
			//~ </div>';
			
	//~ // Par exemple dans le cas du travail de Lorenzo Ruzzene
	//~ return '<div class="origine">
		//~ <p><strong>Genre : ' . $donnees['genre_sexe'] . '<br>
		//~ Sexualité : ' . $donnees['sexualite'] . '<br>
	//~ </div>

	//~ <div class="origine">
		//~ <p>Pays de publication : ' . $donnees['pays'] . '<br>
		//~ Genre littéraire : ' . $donnees['genre_litt'] . '<br><br>
	//~ </div>';
}

// 3. b. Tri dans la base de données. Ajouter les éléments pertinents pour vous. Ajouter par exemple 'periode', 'pdv', 'corpus', 'noyau', 'cinema', 'sexualite'... en cohérence avec vos choix précédents.
function var_recherche_criteres_tri() {
	return array('pays', 'genre', 'origine_edition', 'titre');
}


// *****************************************************

// 4. GESTION DES DIFFÉRENTES CATÉGORISATIONS DE CORPUS (par défaut il y en a quatre : hors corpus, normal/secondaire, intermédiaire, noyau - déconseillé d'en faire plus, mais on peut en faire moins notamment en supprimant le niveau "théorique")

// 4. a. Au moment d'enregistrer une œuvre. Appelée dans le fichier de modification des données (modif.php) (supprimer la ligne théorie et critique au besoin)
function var_champs_modif_corpus($donnees, $info) {
	return '<label for="corpus">Corpus : ' . $info . '</label>
			<select name="corpus">'
				. check_select($donnees, 'corpus', 'Secondaire', 1)
				. check_select($donnees, 'corpus', 'Théorie et critique', 3)
				. check_select($donnees, 'corpus', 'Noyau', 2)
				. check_select($donnees, 'corpus', 'Hors corpus', 0) . '
			</select><br>';
}

function var_corpus($post) {
	return $post;
}

// 4. b. Au moment d'afficher les vignettes (appelée dans Z_affichage_vignettes.php), indique les couleurs associées à chaque catégorie de corpus (supprimer la ligne "theorique" au besoin)
function var_affich_couleurs($array) {
	if($array['corpus'] == 1) $couleur = 'fonce';
	elseif($array['corpus'] == 2) $couleur= 'noyau';
	elseif($array['corpus'] == 3) $couleur= 'theorique';
	else $couleur = 'clair';
	return $couleur;
}

// 4. c. Au moment d'afficher les critères de recherche (appelée dans Z_criteres_recherche.php), propose des cas à cocher pour sélectionner tel ou tel type de corpus. Supprimer éventuellement l'input qui concerne le corpus théorique.
function var_criteres_checkbox() {
	$d = isset($_POST['noyau']) && $_POST['noyau'] == 1 ? 'checked' : '';
	$e = isset($_POST['theorique']) && $_POST['theorique'] == 1 ? 'checked' : '';
	
	return '<input type="checkbox" value=1 name="noyau" ' .$d . '/><strong class="noyau_checkbox">Noyau primaire</strong><br>
			<input type="checkbox" value=1 name="theorique" ' .$e . '/><strong class="theorique_checkbox">Théorie et critique</strong><br>';	
}

// 4. d. Au moment de trier les citations, utiliser les filtres corpus. Éventuellement supprimer la ligne théorique.
function var_recherche_corpus($ligne) {
	if($ligne['corpus'] == 1) return 'fonce';
	elseif($ligne['corpus'] == 2) return 'noyau';
	elseif($ligne['corpus'] == 3) return 'theorique';
	else return 'clair';
}

function var_condition_noyau() {
	if(isset($_POST['noyau']) && $_POST['noyau'] == 1)
		return 'corpus=2 ';
		
	elseif(isset($_POST['theorique']) && $_POST['theorique'] == 1)
		return 'corpus=3 ';
	
	return '';
	
	//~ // Autre possibilité, selon utilisation
	//~ if(isset($_POST['noyau']) && $_POST['noyau'] == 1)
		//~ return 'noyau=1 ';
	//~ return '';
}

// *****************************************************

// 5. CRITÈRES DE RECHERCHE DANS LA PAGE DU PLAN DE TRAVAIL

// 5. a. Appelée dans Z_criteres_recherche.php. Par défaut, Litote cherche toutes les citations stockées dans tel ou tel chapitre (il est possible de lui demander autre chose). Si vous souhaitez changer le mode de recherche par défaut pour "voir prioritairement les citations déjà utilisées dans la rédaction", inversez le commentaire pour régler le paramètre sur "utilise". Éventuellement utile en fin de travail, pas au début.
function var_critere_stock() {			
	return 'stock';
	//~ return 'utilise';
}




// *****************************************************
// *****************************************************

// À NE CHANGER QUE DANS LE CAS D'UNE MISE EN PLACE DIFFÉRENTE ENTRE DÉMO PUBLIQUE ET USAGE PRIVÉ ***********************************************************************

// Servent à indiquer les bons liens et titres (selon démo ou privé)
function var_index_demo() { return 'index.php'; }
function var_contexte_menu() { return 'index'; } // Appelées dans index.php
function var_menu_modif() { return 'modif'; } // Appelées dans modif.php
function var_menu_oeuvres() { return 'oeuvres'; } // Appelée dans oeuvres.php
function var_menu_projets() { return 'projets'; } // Appelées dans projets.php
function var_menu_plan() { return 'plan'; } // Appelées dans vue-plan.php
function var_titre_page_projets() { return 'Projets'; } // Appelée dans Z_fonctions_transversales.php


// En début de pages, sert à demander un mot de passe ou non à l'utilisateurice (inutile pour la démo, laisser pour la version de travail)
function var_verif_mdp() {
	if((isset($_SESSION['mdp']) && $_SESSION['mdp'] == var_mdp())
	|| (isset($_POST['mdp']) && $_POST['mdp'] == var_mdp()))
	{
	$_SESSION['mdp'] = var_mdp();
	}
}

// Dans la page de modification des citations, à modifier si vous souhaitez pouvoir enregistrer les numéros de page d'utilisation des citations (dans le cas d'un ouvrage achevé) et les éventuelles url qui s'y rattachent
function var_modif_stock($titre, $id, $value, $valueb) {
	
	return '<br>' . $titre . ' (<span style="font-size:0.8em;"><input type="checkbox" value="1" name="' . $id . '"' . $value . '/> Stocker <input type="checkbox" value="1" name="' . $id . '_ver"' . $valueb . '/> Confirmer l\'utilisation</span>)';
	
	//~ return '<div class="cite_dans">'	
		//~ . m_input($citation, $id . '_ver', 'Citation confirmée p.')
		//~ . m_input($citation, $id . '_ver_url', 'Url') . '
		//~ </div>';
}

// Indiquer un message d'avertissement aux utilisateurices de la base de données (par exemple pour rappeler que le pillage, c'est une pratique scientifique douteuse). Par défaut vide, renseignez votre propre texte. (appelées dans Z_fonctions_transversales.php)
function var_info_pillage() { return ''; } 


// Fonctions de mise à jour de la base de données (appelées dans Z_updates_bdd.php) : à ne modifier que pour le cas d'une mise en ligne publique où les utilisateurices n'auraient pas le droit de toucher aux données

function var_updates_suppr($type, $don) {
	global $bdd;

	if($type == 'projet') $suppr = $bdd->query('ALTER TABLE citations DROP projet' . $_POST['ID_projet']);

	elseif($type == 'tags') $suppr = $bdd->query('ALTER TABLE citations DROP ' . $don['tag_bdd']);
	
	elseif($type == 'chapitre' && !isset($_POST['suppr_partie'])) $suppr = $bdd->query('ALTER TABLE citations DROP chap' . $_POST['ID_chapitre'] . ',
			DROP chap' . $_POST['ID_chapitre'] . '_ver,
			DROP chap' . $_POST['ID_chapitre'] . '_ver_url');
					
	elseif($type == 'oeuvre') $suppr = $bdd->query('DELETE FROM citations WHERE ID_oeuvre = ' . $_POST['ID_oeuvre']);
	
	elseif($type == 'classement') {
		$numero = $bdd->query('SELECT ordre FROM classementtags WHERE ID_classement = ' . $_POST['ID_classement']);
		if($donnees = $numero->fetch(PDO::FETCH_ASSOC))
		{
			$suppr = $bdd->query('UPDATE tags SET ordre=0 WHERE ordre = ' . $donnees['ordre']);
		}
		$numero->closeCursor();	
	}
}

function var_updates_suppr_item($types, $i) {
	global $bdd;
	$suppr = $bdd->prepare('DELETE FROM ' . $types[$i]['table'] . ' WHERE ' . $types[$i]['id'] . ' = ?');
	$suppr->execute(array($_POST[$types[$i]['id']]));
}

function var_updates_panier_projet($id, $id_cit) {
	global $bdd;
	$updatepanier = $bdd->query('UPDATE citations SET projet' . $id . '=1 WHERE ID_citation= ' . $id_cit);
}

function var_updates_panier_chapitre_ou_tags($id_cit, $condition) {
	global $bdd;
	
	if(isset($_SESSION['mdp']) && $_SESSION['mdp'] == var_mdp()) {
		$updatepanier = $bdd->query('UPDATE citations SET ' . $condition . ' WHERE ID_citation= ' . $id_cit);
	}
	else echo '<script>window.location.href = "modif.php";</script>';
}

function var_updates_requete($requete) {
	global $bdd;
	//~ print_r($requete);
	$reponse = $bdd->query($requete);
	$reponse->closeCursor();
}

function var_updates_spec($post) {
	return $post;
}

function var_updates_ajout($type, $id_projet) {
	global $bdd;
	
	if($type == 'projet') $col = 'projet' . $id_projet;
	elseif($type == 'tag') $col = $id_projet;
	
	$creercolonnes = $bdd->query('ALTER TABLE citations
		ADD ' . $col . ' INT NOT NULL DEFAULT FALSE');
}

function var_create_chap($cle) {
	global $bdd;
	
	$creercolonnes = $bdd->query('ALTER TABLE citations
		ADD chap' . $cle['ID_chapitre'] . ' INT NOT NULL DEFAULT \'0\',
		ADD chap' . $cle['ID_chapitre'] . '_ver VARCHAR (70) DEFAULT \'0\',
		ADD chap' . $cle['ID_chapitre'] . '_ver_url VARCHAR (380) DEFAULT NULL');
}

function var_updates_demo($ctxt, $type, $don, $id) {
	if($ctxt == 'ajout') return 'L\'ajout a bien été enregistré.';
	if($ctxt == 'modif') return 'La modification a bien été enregistrée.';
	if($ctxt == 'doublon') return '<p class="left">Il n\'y a pas eu d\'enregistrement : les données existaient déjà dans la base.</p>
			<form class="left" method="get" action="modif.php">
				<input type="hidden" value=' . $don[$id] . ' name="modif_' . $type . '"/>
				<input type="submit" class="bouton_final" value="Modifier les données"/>
			</form>';
}

?>
