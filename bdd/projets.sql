-- Adminer 4.8.1 MySQL 10.11.4-MariaDB-1~deb12u1 dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `projets`;
CREATE TABLE `projets` (
  `ID_projet` int(11) NOT NULL AUTO_INCREMENT,
  `titre` varchar(500) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL DEFAULT 'Titre non défini',
  `type` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL DEFAULT 'brouillon',
  `ref_biblio` text CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `date` varchar(30) NOT NULL DEFAULT 'Date inconnue',
  `chap` varchar(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID_projet`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

INSERT INTO `projets` (`ID_projet`, `titre`, `type`, `ref_biblio`, `date`, `chap`) VALUES
(1,	'Exemple brouillon',	'brouillon',	'',	'2023-11',	0),
(2,	'Exemple publication',	'publication',	'',	'2023-11',	0),
(3,	'Exemple annexe',	'annexe',	'',	'2023-11',	0);


-- 2023-11-07 16:22:34
