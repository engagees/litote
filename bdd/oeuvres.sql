-- Adminer 4.8.1 MySQL 10.11.4-MariaDB-1~deb12u1 dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `oeuvres`;
CREATE TABLE `oeuvres` (
  `ID_oeuvre` int(11) NOT NULL AUTO_INCREMENT,
  `corpus` tinyint(1) NOT NULL,
  `prenom` varchar(2000) NOT NULL,
  `nom` varchar(2000) NOT NULL,
  `titre` varchar(250) NOT NULL DEFAULT 'Titre à reprendre',
  `nationalite` varchar(50) NOT NULL DEFAULT '',
  `genre_sexe` varchar(50) NOT NULL DEFAULT '',
  `sexualite` varchar(50) NOT NULL DEFAULT '',
  `pdv` varchar(50) NOT NULL DEFAULT '',
  `periode` varchar(50) NOT NULL DEFAULT '',
  `type` varchar(50) NOT NULL DEFAULT '',
  `genre_litt` varchar(50) NOT NULL DEFAULT '',
  `courant_date` varchar(100) NOT NULL,
  `courant_lieu` varchar(100) NOT NULL,
  `courant_edition` varchar(100) NOT NULL,
  `origine_date` varchar(100) NOT NULL,
  `origine_lieu` varchar(100) NOT NULL,
  `origine_edition` varchar(100) NOT NULL,
  `resume` longtext NOT NULL,
  `reflexions` longtext NOT NULL,
  PRIMARY KEY (`ID_oeuvre`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

INSERT INTO `oeuvres` (`ID_oeuvre`, `corpus`, `prenom`, `nom`, `titre`, `nationalite`, `genre_sexe`, `sexualite`, `pdv`, `periode`, `type`, `genre_litt`, `courant_date`, `courant_lieu`, `courant_edition`, `origine_date`, `origine_lieu`, `origine_edition`, `resume`, `reflexions`) VALUES (NULL, '1', 'Prénom', 'Nom', 'Œuvre à supprimer (initialisation)', 'Pays', 'Genre', 'Sexualité', 'Point de vue', 'Période', 'Type', 'Genre littéraire', '2023', 'Lieu de l\'édition utilisée', 'Édition utilisée', '2023', 'Lieu de l\'édition originale', 'Édition originale', 'Résumé', 'Notes'); 

-- 2023-11-07 16:22:31
