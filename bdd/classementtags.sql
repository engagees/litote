-- Adminer 4.8.1 MySQL 10.11.4-MariaDB-1~deb12u1 dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `classementtags`;
CREATE TABLE `classementtags` (
  `ID_classement` int(11) NOT NULL AUTO_INCREMENT,
  `titre` varchar(150) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `ordre` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`ID_classement`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

INSERT INTO `classementtags` (`ID_classement`, `titre`, `ordre`) VALUES
(0,	'Aucun classement',	0);

-- 2023-11-07 16:22:29
