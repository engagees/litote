-- Adminer 4.8.1 MySQL 10.11.4-MariaDB-1~deb12u1 dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `citations`;
CREATE TABLE `citations` (
  `ID_citation` int(11) NOT NULL AUTO_INCREMENT,
  `ID_oeuvre` int(11) NOT NULL,
  `citation` longtext NOT NULL,
  `page` varchar(11) NOT NULL,
  `contexte` text NOT NULL,
  `ref_these` varchar(150) NOT NULL DEFAULT '0',
  `url_these` text DEFAULT NULL,
  `init` tinyint(1) NOT NULL DEFAULT 0,
  `chap1` tinyint(1) NOT NULL DEFAULT 0,
  `chap1_ver` varchar(70) NOT NULL DEFAULT '0',
  `chap1_ver_url` text DEFAULT NULL,
  `chap2` tinyint(1) NOT NULL DEFAULT 0,
  `chap2_ver` varchar(70) NOT NULL DEFAULT '0',
  `chap2_ver_url` text DEFAULT NULL,
  `chap3` tinyint(1) NOT NULL DEFAULT 0,
  `chap3_ver` varchar(70) NOT NULL DEFAULT '0',
  `chap3_ver_url` text DEFAULT NULL,
  `projet1` tinyint(1) NOT NULL DEFAULT 0,
  `projet2` tinyint(1) NOT NULL DEFAULT 0,
  `projet3` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`ID_citation`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

INSERT INTO `citations` (`ID_citation`, `ID_oeuvre`, `citation`, `page`, `contexte`, `ref_these`, `url_these`, `chap1`, `chap1_ver`, `chap1_ver_url`, `chap2`, `chap2_ver`, `chap2_ver_url`, `chap3`, `chap3_ver`, `chap3_ver_url`, `projet1`, `projet2`, `projet3`) VALUES (NULL, '1', 'Citation à supprimer (initialisation)', '1', 'Notes', '', '', '0', '0', '', '0', '0', '', '0', '0', '', '0', '0', '0'); 

-- 2023-11-07 16:22:26
