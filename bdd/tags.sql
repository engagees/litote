-- Adminer 4.8.1 MySQL 10.11.4-MariaDB-1~deb12u1 dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `tags`;
CREATE TABLE `tags` (
  `ID_tag` int(11) NOT NULL AUTO_INCREMENT,
  `tag_complet` varchar(100) NOT NULL,
  `tag_bdd` varchar(50) NOT NULL,
  `ID_classement` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`ID_tag`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

INSERT INTO `tags` (`ID_tag`, `tag_complet`, `tag_bdd`, `ID_classement`) VALUES (NULL, 'Tag pour initialisation (à supprimer)', 'init', '0'); 

-- 2023-11-07 16:22:37
