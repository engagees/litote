-- Adminer 4.8.1 MySQL 10.11.4-MariaDB-1~deb12u1 dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `chapitres`;
CREATE TABLE `chapitres` (
  `ID_chapitre` int(11) NOT NULL AUTO_INCREMENT,
  `partie` int(11) NOT NULL,
  `chap` varchar(15) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `titre` varchar(215) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `titre_court` varchar(150) NOT NULL,
  PRIMARY KEY (`ID_chapitre`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;


INSERT INTO `chapitres` (`ID_chapitre`, `partie`, `chap`, `titre`, `titre_court`) VALUES
(1,	0,	'intro',	'Introduction',	'introduction'),
(2,	4,	'conclu',	'Conclusion',	'conclusion'),
(3,	5,	'annexes',	'annexes',	'annexes');


-- 2023-11-07 16:22:13
