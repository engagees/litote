<SCRIPT language=javascript>
	window.fen = null;
	function openPopup(contexte){       
			var lien = "a-propos.php?mode-d-emploi=" + contexte
            window.fen=window.open(lien,"mode-d-emploi","width=800, height=800 top=50, left=50");
	}
	function closePopup(){
        window.fen.close();
	}

	function openPanier(id){   
		var lien = "panier.php?remplir_panier=" + id    
		window.fen=window.open(lien, "remplir_panier", "width=500, height=500 top=50, left=50");
	}
	function closePanier(){
	window.fen.close();
	}
</SCRIPT>

<?php

if (!function_exists('str_contains')) {
    function str_contains($haystack, $needle) {
        return $needle !== '' && mb_strpos($haystack, $needle) !== false;
    }
 }
 
function explications_markdown() {
	return 'Il s\'agit de transcrire les italiques avec des tirets underscore (exemple : _un italique_), les gras avec des doubles étoiles (exemple : **un gras**), les liens avec crochets et guillemets (exemple : [appel de lien](https://adressedulien.fr)). Les sauts de ligne sont pris en compte et certains raccourcis sont prévus pour faciliter la tâche : “<<” est par exemple retranscrit en “[...]”.';
}

function si_utilisee($array, $key) {
			
	global $bdd;
	
	$each_chap[0] = $each_chap[1] = '';
	
	// Pour l'introduction
	
	$reponse = $bdd->query('SELECT * FROM chapitres WHERE chap="intro"');

	while($donnees = $reponse->fetch(PDO::FETCH_ASSOC))
	{
		$id = 'chap' . $donnees['ID_chapitre'];
		
		if($key == '_ver' && $array[$id . $key] != '' && $array[$id . $key] != '0') {
					
			$each_chap[0] .= 'introduction, 
				<a target="_blank" href="' . $array[$id . '_ver_url'] . '">p.&#8239;' 
				. $array[$id . '_ver'] . '</a>';
		}	
		elseif($key != '_ver' && $array[$id] != 0) {
			
			$each_chap[1] .= 'introduction';
		}
	}
	$reponse->closeCursor();
	
	// Pour les chapitres
	
	$reponse = $bdd->query('SELECT * FROM chapitres WHERE chap="partie" ORDER BY partie');

	while($donnees = $reponse->fetch(PDO::FETCH_ASSOC))
	{
		$reponse2 = $bdd->query('SELECT * FROM chapitres WHERE chap="chapitre" AND partie="' . $donnees['partie'] . '" ORDER BY titre');
		
		while($don = $reponse2->fetch(PDO::FETCH_ASSOC))
		{
			$id = 'chap' . $don['ID_chapitre'];
		
			if($key == '_ver' && $array[$id . $key] != '' && $array[$id . $key] != '0') {
					
				$virgule = $each_chap[0] != '' ? '&#8239;; ' : '';
				
				//~ if($key == '_ver') {
					$each_chap[0] .= $virgule . $don['titre_court'] 
					. ', <a target="_blank" href="' . $array[$id . '_ver_url'] . '"
					title="Le numéro de page indiqué renvoie au fichier officiel de la thèse, dans laquelle est mobilisée cette citation. Le lien n\'est pas encore actif : il le sera lorsque la thèse sera mise en ligne, quelques mois après la soutenance, et renverra directement à la page concernée.">p.&#8239;' 
					. $array[$id . '_ver'] . '</a>';
				//~ }
				
				if($array[$id] != 0) {
				//~ elseif($array[$id] != 0) {
					
					$virgule = $each_chap[1] != '' ? '&#8239;; ' : '';
					$each_chap[1] .= $virgule . $don['titre_court'];
				}
			}
		}
		$reponse2->closeCursor();
	}
	$reponse->closeCursor();
	
	// Pour la conclusion
	
	$reponse = $bdd->query('SELECT * FROM chapitres WHERE chap="conclu"');

	while($donnees = $reponse->fetch(PDO::FETCH_ASSOC))
	{
		$id = 'chap' . $donnees['ID_chapitre'];
		
		if($key == '_ver' && $array[$id . '_ver'] != '' && $array[$id . '_ver'] != '0') {
					
			$virgule = $each_chap[0] == null ? '' : '&#8239;; ';	
			$each_chap[0] .= $virgule . 'conclusion, 
				<a target="_blank" href="' . $array[$id . '_ver_url'] . '">p.&#8239;' 
				. $array[$id . '_ver'] . '</a>';
		}	
		elseif($key != '_ver' && $array[$id] != 0) {
			
			$each_chap[1] .= 'conclusion';
		}
	}
	$reponse->closeCursor();
	
	// Pour les annexes
	
	$reponse = $bdd->query('SELECT * FROM chapitres WHERE chap="annexes"');
	
	while($donnees = $reponse->fetch(PDO::FETCH_ASSOC))
	{
		$id = 'chap' . $donnees['ID_chapitre'];
		
		if($key == '_ver' && $array[$id . '_ver'] != '' && $array[$id . '_ver'] != '0') {
				
			$virgule = $each_chap[0] == null ? '' : '&#8239;; ';	
			$each_chap[0] .= $virgule . 'annexes, 
				<a href="' . $array[$id . '_ver_url'] . '">p.&#8239;' 
				. $array[$id . '_ver'] . '</a>';
		}
	}
	$reponse->closeCursor();
	
	return $each_chap;
}

function a_propos($a_expliquer) {
	return '<form class="settings menu_a_propos" method="get" onclick="openPopup(\'' . $a_expliquer . '\');">
		<input type="hidden" name="mode-d-emploi" value="' . $a_expliquer .'"/>
		<input class="a-propos" type="submit" title="Cliquer ici pour ouvrir le pop-up de mode d\'emploi." value="?"/>
	</form>';
}

function infobulle($texte) {
	return '<span class="infobulle">
				<input type="button" title="' . $texte . '" class="infobulle_symb" value="❓"/>
			</span>';
}

function footer() {
	return '<footer>
	<span>Administration du site : Aurore Turbiau. Contact : <a href="mailto:aurore.turbiau@zaclys.net">aurore.turbiau [at] zaclys.net</a></span>. 
	
	<form class="inline" method="get" onclick="openPopup(\'modalites-legales\');">
		<input type="hidden" name="mode-d-emploi" value="modalites-legales">
		<input type="submit" value="Voir les modalités légales.">
	</form>
	
	</footer>';
}

function menu($lieu) {

	$lien_accueil = str_contains($lieu, 'demo') ? 'baseAuroreTurbiau' : 'index';
	
	$logo = '<div class="logo"><a href="' . $lien_accueil . '.php"><img class="logo" src="Logo2-Litote-avec-nom-rose-sans-contours.png"/></a></div>';
	
	$lien_plan = 'vue-plan';
	$lien_projets = 'projets';
	$lien_modif = 'modif';

	echo '<header>
		<div class="header_inside">' 
		. $logo 
		. '<div class="menu">
		<form method="post" action="modif.php">
		<input class="menu_nouvelle" type="submit" name="nouvelle_oeuvre" value="Nouvelle œuvre ?"/>
	</form>

	<form method="post" action="modif.php">
		<input class="menu_nouvelle" type="submit" name="nouvelle_citation" value="Nouvelle citation ?"/>
	</form>';
	
	echo '<span class="texte-header"></span>
		
	<form class="menu_recherche" method="post" action="' . $lien_accueil . '.php">
		<input type="text" class="menu_barre_recherche" name="textearechercher"/>
		<input class="margin-left menu_chercher" type="submit" value="Chercher"/>
	</form>';
		
	echo '<span class="menu_liens">
	<span class="texte-header menu_plan"><a href="vue-plan.php">Plan</a></span>
			
	<span class="texte-header menu_projets"><a href="projets.php">' . var_titre_page_projets() . '</a></span></span>

	<span class="menu_icones">
	<span class="settings menu_settings" ><a href="modif.php" title="Cliquer ici pour aller dans l\'espace de configuration de Litote">&#9881;</a></span>';
	
	if(empty($_SESSION['panier'])) echo '<span class="settings menu_panier"><a title="Le panier est vide" onclick="openPanier();"><strong>⦾</strong></a></span>';
	else echo '<span class="settings"><a title="Le panier est plein de citations" onclick="openPanier();" style="color:#d51079;"><strong>◉</strong></a></span>';
	
	echo a_propos($lieu);
	
	echo '</span></div>
	</div>
	
	</header>
	
	<div class="margin-top-cause-menu"></div>';
}

function checked($name) {
	return isset($_POST[$name]) && $_POST[$name] != 0 ? 'checked' : ''; }

// Vérifie s'il y a une ou plusieurs autrices ***********************

function check_multi_auteurs($donnees)
{
	global $bdd;
	
	// Affichage des noms d'autrices quand il y en a plusieurs **************************************

	if(is_array($donnees))
	{
		if(!str_contains($donnees['nom'], ';')) return $donnees['prenom'] . ' ' . $donnees['nom'];
		
		else
		{
			$autrice = '';
			foreach(explode(';', $donnees['prenom']) as $id => $prenom)
			{
				$autrice .= $prenom . ' ' . explode(';', $donnees['nom'])[$id] . ', ';
			}
			return $autrice;
		}
	}
	
	// Sélection des autrices dans la liste déroulante d'accueil ************************************
	
	if($donnees == 'choix')
	{
		$reponse = $bdd->query('SELECT DISTINCT nom, prenom FROM oeuvres ORDER BY nom');

		$autrices = array();
		while($donnees = $reponse->fetch())
		{

			if(!str_contains($donnees['nom'], ';')) {
				$autrices[] = $donnees['nom'] . ';' . $donnees['prenom'];
			}
			
			else
			{
				$tmp_nom = explode(';', $donnees['nom']);
				foreach(explode(';', $donnees['prenom']) as $id => $prenom)
				{
					$autrices[] = $tmp_nom[$id] . ';' . $prenom;
				}
			}
		}
		$reponse->closeCursor();
		
		$autrices = array_unique($autrices);
		asort($autrices);
		
		$a = '';
		foreach($autrices as $id => $autrice)			
		{
			$b = isset($_POST['autrice']) && $_POST['autrice'] == $autrice ? ' selected' : '';			
			$a .= '<option value="' . $autrice . '"' . $b . '>' . str_replace(';', ' ', $autrice) . '</option>';
		}
		
		return $a;
	}
}

// Appelée sur plusieurs pages : vérifie qu'il n'y a pas d'entourloupe ******************************

function secu($string, $quote)
{
	global $bdd;
	
	// Traduire le markdown en html******************************************************************
	
	$caracteres = 'a-zA-Z0-9àùéèçêüë?!<>';	
	$pattern = array(
		'/\n([^<])/', 
		'/\<\</', 
		'/« /', 
		'/ »/', 
		'/«/',
		'/»/',
		'/\'/',		
		'/\$\$([' . $caracteres . '])/',
		'/([' . $caracteres . '])\$\$/',
		'/\*\*([' . $caracteres . '])/',
		'/([' . $caracteres . '.?!])\*\*/',		
		'/\_([' . $caracteres . '])/',
		'/([' . $caracteres . '.?!])\_/',		 
		'/(^=)"([' . $caracteres . '])/',
		'/([' . $caracteres . '.?!])"(^>)/',
		'/\[([^\]]+)\]\(([^\)]+)\)/'
	);

	$replacement = array(
		'<br>$1', 
		'[...]', 
		'“', 
		'”', 
		'“',
		'”',
		'’',
		'<span style="background-color:#FBEFFB;">$1',
		'$1</span>',
		'<strong>$1',
		'$1</strong>',
		'<em>$1',
		'$1</em>',
		'$1“$2',
		'$1”$2',
		'<a href="$2">$1</a>'
	);

	$string = preg_replace($pattern, $replacement, $string);
	
	// On regarde si le type de string est un nombre entier (int)
	if(ctype_digit($string))
	{
		$string = intval($string);
	}
	// Pour tous les autres types
	else
	{
		$string = $quote == 'quote' ? $bdd->quote($string) : $string;
		$string = addcslashes($string, '%_');
	}
	
	return $string;
}

// Vérifie si une case doit etre pré-cochée ou non *************************************************
function check_conditions($var, $label, $style)
{
	$a = isset($_POST[$var]) && $_POST[$var] != 0 ? 'checked' : '';
	
	return '<input type="checkbox" value=1 name="' . $var . '" ' .$a . '/><span style="' . $style . '">' . $label . '</span><br>';
}

// Vérifie si une option doit être présélectionnée ou pas********************************************
function check_select($donnees, $bdd, $label, $value)
{
	if(isset($donnees[$bdd]) && $donnees[$bdd] == $value)
		$a = 'selected';
	else $a = '';
	
	return '<option value="' . $value . '" ' . $a . '>' . $label . '</option>';
}

// Gère les critères de recherche à choix multiples (titres, auteurices, etc. applicable à différents projets)

function criteres_choix_multiples_simple($name_bdd, $label) {
	global $bdd;
	
	$a = '<div class="options">		
		<select name="' . $name_bdd . '">
			<option value="null">' . $label . '</option>';
			
	$reponse = $bdd->query('SELECT DISTINCT ' . $name_bdd . ' FROM oeuvres ORDER BY ' . $name_bdd);
	while($donnees = $reponse->fetch())
	{
		$c = isset($_POST[$name_bdd]) && $_POST[$name_bdd] == $donnees[$name_bdd] ? 'selected' : '';
		$a .= '<option value="' . $donnees[$name_bdd] . '"' . $c . '>' . ucfirst($donnees[$name_bdd]) . '</option>';
	}
	$reponse->closeCursor();
	
	$a .= '</select>
	</div>';
	
	return $a;	
}

?>

