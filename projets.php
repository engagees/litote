<?php session_start();?>

<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8" />
	<?php include 'Z_meta_variations.php';?>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="robots" content="noindex, nofollow,noarchive">
	<link rel="stylesheet" href="style_commun.css" />
	<link rel="stylesheet" href="style_plan.css" />
	<link rel="stylesheet" href="style_projets.css" />
	<link rel="stylesheet" href="style_responsive.css" />
	<link rel="icon" type="image/x-icon" href="Litote-logo4-rose.ico" />
	<title>Litote - Projets et annexes</title>
</head>

<body>

<?php include 'Z_fonctions_variations.php';?>
<?php include 'Z_connexion.php';?>
<?php include 'Z_updates_bdd.php';?>
<?php include 'Z_fonctions_transversales.php';?>
<?php include 'Z_recherche_citations.php';?>

<div class="container">

<?php menu(var_menu_projets()); ?>

<?php 

	include 'Z_criteres_recherche.php';

// Paramétrer l'enregistrement du panier *********************************************************

echo '<div class="corps_projets">
	
		<div class="formulaire">'
		
	. liste_projets($bdd, 'panier');
	
	echo '</div>';

?>
	
<div class="citation_projets">
<?php
	// Affichage des citations récupérées
	if(isset($_POST['ID_projet'])) include 'Z_affichage_vignettes.php';
?>

</div>

</div>

</div>

<?php echo footer(); ?>

</body>

</html>
