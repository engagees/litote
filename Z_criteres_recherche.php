<?php

// 1. Affichage des critères de sélection des citations************************

function choix_criteres($bdd, $contexte) 
{
	$infobulle_tags = '<div style="position:relative;margin-top:-20px;text-align:right;margin-right:-5px;">' 
		. infobulle('Recherche par étiquettes, en logique exclusive (par défaut, mode “et” : les différents critères cochés seront tous validés dans les citations affichées) ou inclusive (mode “ou” que l\'utilisateurice peut aussi choisir : les citations correspondront à l\'un des critères cochés, parfois l\'un, parfois l\'autre). Le lot d\'étiquette, ici, est propre à un projet spécifique : d\'autres versions de Litote proposent d\'autres étiquettes. Ce lot correspond à une liste finie, décidée au début du projet et qui peut évoluer par la suite, mais qui a intérêt à rester à peu près stable afin que l\'utilisateurice puisse maîtriser son classement. L\'usage des étiquettes, en pratique, est flottant : elles sont vastes, peuvent désigner du contenu thématique comme du contenu formel ou éditorial, parfois plusieurs à la fois.') . '</div>';
	
	/* Affichage choix autrice */
	
	$a = '
	<div class="formulaire">
	<form method="post" action="#">
		<div class="deroul">
	
		<div class="options">
		<select name="autrice">
			<option value="null">' . ucfirst(var_auteurice()) . '</option>
			' . check_multi_auteurs('choix') . '
		</select>
		</div>';
				
		// Affichage choix titre *********************************************************************
		
		$a .= '<div class="options">		
		<select name="titre">
		<option value="null">Titre</option>';
			
		$reponse = $bdd->query('SELECT titre FROM oeuvres ORDER BY titre');
		while($donnees = $reponse->fetch())
		{
			$c = isset($_POST['titre']) && $_POST['titre'] == $donnees['titre'] ? 'selected' : '';
			$a .= '<option value="' . $donnees['titre'] . '"' . $c . '>' . $donnees['titre'] . '</option>';
		}
		$reponse->closeCursor();	
			
		$a .= '</select>
		</div>';
				
		// Affichage choix spécifiques (édition, genre littéraire, période, etc. selon projets) *******************************************************************		
		$a .= var_critere_spec();
		
		if($contexte == 'index') {
		
			// Affichage choix des tags*******************************************************************
			
			$a .= '<div class="tags">
			<div class="tags_globaux">' 
			. check_conditions('tri', 'Tri par « ou »', 'color:#d51079;font-weight:600')
			. var_criteres_checkbox()
			. var_criteres_boolens() . '</div>';	
			
			$reponse = $bdd->query('
				SELECT DISTINCT t.ID_classement, c.ordre, titre
				FROM tags t INNER JOIN classementtags c
				ON t.ID_classement = c.ID_classement
				ORDER BY c.ordre');
			while(($donnees = $reponse->fetch(PDO::FETCH_ASSOC)))
			{
				$classementtags[$donnees['ID_classement']] = array();
				$classementtags[$donnees['ID_classement']]['titre'] = $donnees['titre'];
			}
			$reponse->closeCursor();
			
			if(isset($classementtags)) {
				foreach($classementtags as $key => $classement)
				{
					$a .= $classement['titre'] != 'Aucun classement' ? '<p><strong>' . $classement['titre'] . '</strong><br>' : '';
					
					$reponse = $bdd->query('
					SELECT t.ID_classement, titre, c.ordre, tag_complet, tag_bdd
					FROM classementtags c INNER JOIN tags t
					ON c.ID_classement = t.ID_classement
					WHERE t.ID_classement = ' . $key. '
					ORDER BY c.ordre, tag_complet');	
					while($donnees = $reponse->fetch())
					{
						$c = isset($_POST[$donnees['tag_bdd']]) && $_POST[$donnees['tag_bdd']] == 1 ? 'checked' : '';
						$a .= '<input type="checkbox" value=1 name="' . $donnees['tag_bdd'] . '" ' . $c . '>' . ucfirst($donnees['tag_complet']) . '<br>';
					}
					$reponse->closeCursor();	
					
					$a .= '</p>';
				}
			}
		
			$a .= '<br>' . $infobulle_tags . '</div>';

			$a .= '<input class="chercher" type="submit" value="Chercher">
			
			<input class="modifier" type="submit" name="update_tags_panier" value="Ou mettre à jour les tags sélectionnés avec le contenu du panier"/>';
		}
		
		elseif($contexte == 'plan') {
			
			// Affichage choix des chapitres *************************************************************
			
			$a .= '<div class="chapitres_selection">';
			
			//*****
			
			// Variable à éventuellement changer selon le contexte : si travail, le mieux est de la régler sur "stock", si vitrine, le mieux est de la régler sur "utilise"
			
			$par_defaut = var_critere_stock();
			
			// Fonction de recherche dans tous ces cas
			$reponse = $bdd->query('SELECT * FROM chapitres WHERE chap="intro"');
			while($donnees = $reponse->fetch())
			{
				$a .= check_chapitre($donnees);
			}
			$reponse->closeCursor();
			
			$reponse = $bdd->query('SELECT * FROM chapitres WHERE chap="partie" ORDER BY partie');
			while($donnees = $reponse->fetch())
			{
				$a .= '<br><strong>' . $donnees['titre'] . '</strong><br>';
				
				$reponse2 = $bdd->query('SELECT * FROM chapitres
				WHERE chap="chapitre"
				AND partie="' . $donnees['partie'] . '"
				ORDER BY titre');
				while($don = $reponse2->fetch())
				{
					$a .= check_chapitre($don);
				}
				$reponse2->closeCursor();
			}
			$reponse->closeCursor();
			
			$reponse = $bdd->query('SELECT * FROM chapitres WHERE chap="conclu"');
			while($donnees = $reponse->fetch())
			{
				$a .= '<br>' . check_chapitre($donnees);
			}
			$reponse->closeCursor();
			

			// Critère de tri alternatif au "par défaut"
			
			$stockees = 'STOCK : Voir les citations stockées / stocker des citations';
			$negligees = 'RESTE : Voir les citations stockées mais pas encore utilisées'; 
			$utilisees = 'UTILISÉ : Voir les citations vraiment utilisées / confirmer l\'utilisation des citations';
			
			if($par_defaut == 'stock') {
				$par_defaut_long = $stockees;
				$alt_court = 'utilise';
				$alt_long = $utilisees;
			}
			
			elseif($par_defaut == 'utilise') {
				$par_defaut_long = $utilisees;
				$alt_court = 'stock';
				$alt_long = $stockees;				
			}
			
			$b = $c = $d = '';
			if(isset($_POST['mode-tri-chap'])) { 
				if($_POST['mode-tri-chap'] == 'neglige') $d = 'selected';
				elseif($_POST['mode-tri-chap'] == $par_defaut) $b = 'selected';
				elseif($_POST['mode-tri-chap'] == $alt_court) $c = 'selected';
			}
			
			$a .= '<br><label for="mode-tri-chap"></label>
			
			<select name="mode-tri-chap" id="mode-tri-chap">
				<option value="' . $par_defaut . '">Pour le chapitre sélectionné :</option>
				<option value="' . $par_defaut . '" ' . $b . '>Par défaut - ' . $par_defaut_long . '</option>
				<option value="' . $alt_court . '" ' . $c . '>' . $alt_long . '</option>
				<option value="neglige" ' . $d . '>' . $negligees . '</option>
			</select>';		
			
			$a .= '<br></div>';

			$a .= '<input class="chercher" type="submit" value="Chercher">
			
			<input class="modifier" type="submit" name="update_chapitre" value="Ou mettre à jour le chapitre avec le contenu du panier et le mode d\'enregistrement choisi"/>';
		}
		
	$a .= '</form>
	</div>';
	
	return $a;
}

// FONCTIONS LIÉES AUX PROJETS*************************************************************************

function chercher_projets($bdd, $type) {

	$return = '';
	
	if($type != 'brouillon') {
		
		$where = $type == 'annexe' ? 'type = "annexe"' : 'type="publication" OR type="communication"';
		
		$order = $type == 'annexe' ? 'chap' : 'date DESC';
		
		$return .= '<select style="background-color:white;width:400px;" name="ID_' . $type . '">
			<option value="null">Titre</option>';
			
			$reponse = $bdd->query('SELECT * FROM projets WHERE ' . $where . ' ORDER BY ' . $order);
			while($donnees = $reponse->fetch())
			{
				if(isset($_POST['ID_publication']) && $_POST['ID_publication'] == $donnees['ID_projet']) $a = 'selected';
				elseif(isset($_POST['ID_annexe']) && $_POST['ID_annexe'] == $donnees['ID_projet']) $a = 'selected';
				else $a = '';
				
				$return .=  '<option value="' . $donnees['ID_projet'] . '" ' .$a . '>
					' . $donnees['date'] . ', «&#8239;' . $donnees['titre'] . '&#8239;»</option>';
			}
			$reponse->closeCursor();
			
		$return .= '</select>';
		
	}
	
	else {
		
		$reponse = $bdd->query('SELECT * FROM projets WHERE type="' . $type . '" ORDER BY date DESC');
		while($donnees = $reponse->fetch())
		{
			$a = isset($_POST['ID_brouillon']) && $_POST['ID_brouillon'] == $donnees['ID_projet'] ? 'checked' : '';
			
			$return .=  '<input class="inline" type="radio" value="' . $donnees['ID_projet'] . '" name="ID_brouillon" ' . $a . '>
				<span>' . $donnees['date'] . ', «&#8239;' . $donnees['titre'] . '&#8239;»</span><br>';
		}
		$reponse->closeCursor();
	}
			
	return $return;
}

function liste_projets($bdd, $objectif) {
	
	$a = '<br><br><h3>Critères de recherche</h3>
	<form method="post" action="#">
		
		<div class="liste_projets">
		
		<h4>Annexes</h4>'
		. chercher_projets($bdd, 'annexe') . '
		
		<br><br><H4>Publications et communications</H4>'
		. chercher_projets($bdd, 'publication') . '
				
		<br><br><h4>Brouillons et regroupements temporaires</h4>' 
		. chercher_projets($bdd, 'brouillon') . '
		
		</div><br>
		
		<input type="hidden" name="ID_projet"/>

		<input class="chercher" type="submit" value="Chercher"/>
		<input class="chercher" type="submit" name="update_projet" value="Ou mettre à jour le projet avec le contenu du panier"/>
	</form>
	
	</div>';
	
	return $a;
}

function check_chapitre($donnees) {

	$c = isset($_POST['ID_chapitre']) && ($_POST['ID_chapitre'] == $donnees['ID_chapitre']) ? 'checked' : '';

	return '<input type="radio" value="' . $donnees['ID_chapitre'] . '" name="ID_chapitre" ' . $c . '>' . ucfirst($donnees['titre']) . '<br>';
}

?>
