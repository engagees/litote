<?php

// Trier et afficher les résumés concernés par une recherche globale ***********

function recherche_globale($bdd) 
{
	global $bdd;
	
	if($_POST['textearechercher'] != null) {
	
		$reponse = $bdd->query('SELECT *
			FROM oeuvres 
			WHERE resume LIKE \'%' . addslashes(secu($_POST['textearechercher'], 'no quote')) . '%\'');
	}
		
	if(isset($reponse)) 
	{
		$retour = $reponse->fetchAll();
		$reponse->closeCursor();
		$oeuvres = $retour;
	}
	else $oeuvres = false;
	
	if($oeuvres) {
	
		$a = '<p class="left">« ' . $_POST['textearechercher'] . ' » a été trouvé sur les pages des résumés de ces œuvres :</p>
		<div class="liste_resultats_resumes"><ul>';
		
		foreach($oeuvres as $ligne)
		{
			// Voir s'il y a plusieurs autrices pour un livre ou non *******************************
			$autrice = check_multi_auteurs($ligne);
			
			// Prévoir d'afficher différemment les œuvres si elles sont ou non dans le corpus primaire 
			$couleur = var_recherche_corpus($ligne);
			
			$a .= '<li class="titres-resumes ' . $couleur . '-resumes">
				<a target="_blank" href="oeuvres.php?oeuvre=' . $ligne['ID_oeuvre'] . '">' . $autrice . ', 
				<em>' . $ligne['titre']. '</em>
				</a></li>';
		}
		
		$a .= '</ul></div>';
			
		return $a;
	}
	
	else return '<p class="left">« ' . $_POST['textearechercher'] . ' » n\'a pas été trouvé dans les résumés.</p>';	
}

// 3. Tri des citations qu'on garde, en fonction de la recherche effectuée *************************

function tri_citations($bdd)
{	
	if(isset($_POST['textearechercher']) && $_POST['textearechercher'] != null)
	{
		$reponse = $bdd->query('
		SELECT * 
		FROM citations c LEFT JOIN oeuvres o 
		ON c.ID_oeuvre = o.ID_oeuvre
		WHERE citation LIKE \'%' . addslashes(secu($_POST['textearechercher'], 'no quote')) . '%\'');
	}
	
	elseif(isset($_POST))
	{
		$requete = '
		SELECT * 
		FROM citations c LEFT JOIN oeuvres o 
		ON c.ID_oeuvre = o.ID_oeuvre';
		
		$conditions = array();
		
		if(isset($_POST['ID_projet'])) {
			// Trier en fonction des projets ****************************************************************
			$condition = '';
			$reponse = $bdd->query('SELECT ID_projet FROM projets');
			$count_tags = 0;
			while($donnees = $reponse->fetch())
			{
				$array = array('brouillon', 'publication', 'annexe');
				foreach($array as &$type) {
					$condition .= if_choix_projet($type, $donnees, $condition, $count_tags);
				}
			}
			$condition == '' ? '' : '(' . $condition . ')';	
		}
			
		else {

			//Trier en fonction des chapitres ***************************************************************
			if(isset($_POST['mode-tri-chap'])) {
				$conditions['chapitre'] = condition_chap($bdd, $_POST['mode-tri-chap']);
			}
			
			// Trier en fonction des tags *******************************************************************
			$conditions['tag'] = condition_tag($bdd);
		
			// Trier pour les autres critères ***************************************************************
			foreach(var_recherche_criteres_tri() as $critere)
			{
				$conditions[$critere] = condition_string($critere);
			}
			
			$conditions['autrice'] = condition_autrice();
			$conditions = var_condition_booleens($conditions);

			$conditions['noyau'] = var_condition_noyau();
			
			$condition = '';
			foreach($conditions as $cond)
			{
				if($cond != '')
				{
					if($condition == '')
						$condition = ' WHERE ';
					else
						$condition .= ' AND ';
					$condition .= $cond;
				}
			}
		}
		if(str_contains($condition, 'WHERE')) 
			$reponse = $bdd->query($requete . $condition);
	}
	
	if(isset($reponse)) 
	{
		$retour = $reponse->fetchAll();
		$reponse->closeCursor();
		return $retour;
	}
	
	return false;			
}

// 3. a. Fonction appelée pour le tri des citations, vérifier selon les tags************************

function condition_tag($bdd)
{
	$condition = '';
	$mode_ou = isset($_POST['tri']) && $_POST['tri'] == 1;
	$reponse = $bdd->query('SELECT tag_bdd FROM tags');
	$count_tags = 0;
	while($donnees = $reponse->fetch())
	{
		if(isset($_POST[$donnees['tag_bdd']]))
		{
			if($count_tags++ != 0)
			{
				if($mode_ou) /* Mode OU */
					$condition .= ' OR ';
				else /* Mode ET */
					$condition .= ' AND ';
			}
			$condition .= $donnees['tag_bdd'] . '=1';
		}
	}
	return $condition == '' ? '' : '(' . $condition . ')';
}

// 3. b. Fonction appelée pour le tri des citations, vérifier selon le nom d'autrice.

function condition_autrice() // A savoir : pas parfait.
{
	if(isset($_POST['autrice']) && $_POST['autrice'] != 'null')
	{
		$nomprenom = explode(';', $_POST['autrice']);
		$nom = $nomprenom[0];
		$prenom = $nomprenom[1];
		return 'nom LIKE "%' .$nom . '%" AND prenom LIKE "%' . $prenom . '%"';
	}
	return '';
}

// 3. c. Fonction appelée pour le tri des citations, vérifier selon les autres critères de recherche

function condition_string($cond)
{
	if(isset($_POST[$cond]) && $_POST[$cond] != 'null')
		return $cond . '="' . $_POST[$cond] . '"';
	
	return '';
		
}

// 3. d. Fonction appelée pour le tri selon chapitres ou pas

function condition_chap($bdd, $mode_tri)
{
	$condition = $condition_neglige = '';
	$count_tags = 0;
		
	$reponse = $bdd->query('SELECT ID_chapitre FROM chapitres WHERE chap = "chapitre" OR chap = "intro" OR chap="conclu"');		
	while($donnees = $reponse->fetch())
	{		
		$id = 'chap' . $donnees['ID_chapitre'];
		
		if($mode_tri == 'neglige') $check = '=1 AND ' . $id . '_ver=\'\'';
		elseif($mode_tri == 'utilise') $check = '_ver != \'\'';	
		elseif($mode_tri == 'stock') $check = '=1';
		
		if(isset($_POST['ID_chapitre']) && $_POST['ID_chapitre'] == $donnees['ID_chapitre'])
		{
			if($count_tags++ != 0) $condition .= ' AND ';
			$condition .= $id . $check;
		}
	}
	
	return $condition == '' ? '' : '(' . $condition . ')';
}

// 3. f. Fonction appelée pour le tri selon projets ou pas

function if_choix_projet($type, $donnees, $condition, $count_tags) {
	
	if(isset($_POST['ID_' . $type]) && $_POST['ID_' . $type] == $donnees['ID_projet'])
	{
		if(
		($type == 'publication'
			&& $_POST['ID_annexe'] == 'null'
			&& !isset($_POST['brouillon']))
		||
		($type == 'brouillon'
			&& $_POST['ID_annexe'] == 'null'
			&& $_POST['ID_publication'] == 'null')
		||
		($type == 'annexe'
			&& $_POST['ID_publication'] == 'null'
			&& !isset($_POST['brouillon']))
		) {
			
			if($count_tags++ != 0) $condition .= ' AND ';
			$condition .= ' WHERE projet' . $donnees['ID_projet'] . '=1';
		}
		
		else $condition = '';
		
		return $condition;	
	}
}



?>
