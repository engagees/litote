<?php session_start();?>

<!DOCTYPE html>
<html>
	
	<?php include 'Z_fonctions_variations.php';?>

<head>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="robots" content="noindex, nofollow,noarchive">
	<link rel="stylesheet" href="style_commun.css" />
	<link rel="stylesheet" href="style_main.css" />
	<link rel="stylesheet" href="style_demo.css" />
	<link rel="stylesheet" href="style_responsive.css" />
	<link rel="icon" type="image/x-icon" href="Litote-logo4-rose.png" />
	<title>Panier</title>
</head>

<body>
	
<?php include 'Z_connexion.php';?>
<?php include 'Z_fonctions_transversales.php';?>

<div class="panier">
	
	<h2>Panier</h2>

<?php

// GESTION DES PANIERS TEMPORAIRES **********************************************************************

if(!isset($_SESSION['panier'])) {
	$_SESSION['panier'] = array();
	$a = true;
}

// Compléter le panier s'il y en a un d'ouvert **********************************************************
if(isset($_GET['remplir_panier']) &&
	$_GET['remplir_panier'] != 'undefined' &&
	array_search($_GET['remplir_panier'], $_SESSION['panier']) === false) 
{
	$b = true;
	array_push($_SESSION['panier'], $_GET['remplir_panier']);
}

// Si les paramètres du panier sont déjà ok, fermer automatiquement l'onglet d'enregistrement ***********
if(!isset($a) && isset($b))	echo '<script>window.close();</script>';

// S'il faut vider le panier ****************************************************************************
if(isset($_POST['vider_panier'])) unset($_SESSION['panier']);



/////////////////////////////////////////////////////////////////////////


if(empty($_SESSION['panier'])) $panier = 'Le panier est actuellement vide.';
else {
	$panier = '<br>
	<form method="post" action="#">
	<label for="vider_panier">Il y a des citations dans le panier.</label>
	<input class="chercher" type="submit" name="vider_panier" value="Vider le panier"/>
	</form>';
} 

echo '<div class="corps_projets">
	
	<div class="formulaire">' . $panier . '
	
	<br><a class="modifier" href="panier.php">Rafraîchir la fenêtre</a>';
	
echo '</div>';

?>

</div>

</body>

</html>
