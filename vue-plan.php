<?php session_start();?>

<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8" />
	<?php include 'Z_meta_variations.php';?>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="robots" content="noindex, nofollow,noarchive">
	<link rel="stylesheet" href="style_commun.css" />
	<link rel="stylesheet" href="style_plan.css" />
	<link rel="stylesheet" href="style_responsive.css" />
	<link rel="icon" href="Litote-logo4-rose.png" />
	<title>Litote - Plan</title>
</head>

<body>
	
<?php include 'Z_fonctions_variations.php';?>
<?php include 'Z_connexion.php';?>
<?php include 'Z_updates_bdd.php';?>
<?php include 'Z_fonctions_transversales.php';?>
<?php include 'Z_recherche_citations.php';?>

<div class="container">

<?php menu(var_menu_plan()); ?>

<div class="choix">
<?php
	// Affichage panneau du choix des critères de sélection ****************************************
	include 'Z_criteres_recherche.php';
	echo choix_criteres($bdd, 'plan');
?>
</div>

<div class="corps">
		
<?php 
	// Affichage des résultats de la recherche de citation : résumé puis vignettes******************
	echo '<div class="resultats_recherche_globale">';
		
		if(isset($_POST['textearechercher'])) echo recherche_globale($bdd);	
			
	echo '</div>
	
	<div class="citation">';
	
		include 'Z_affichage_vignettes.php';

	echo '</div>';
?>
		
</div>

</div>

<?php echo footer(); ?>

</body>

</html>
