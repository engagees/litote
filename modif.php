<?php session_start(); ?>

<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8" />
	<?php include 'Z_meta_variations.php';?>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="robots" content="noindex, nofollow,noarchive">
	<link rel="stylesheet" href="style_main.css" />
	<link rel="stylesheet" href="style_commun.css" />
	<link rel="stylesheet" href="style_responsive.css" />
	<link rel="stylesheet" href="<?php echo var_style();?>" />
	<link rel="icon" href="Litote-logo4-rose.png" />
	<title>Litote - modifications</title>
</head>

<body>
	
<div class="container">

<?php include 'Z_fonctions_variations.php';?>
<?php include 'Z_connexion.php';?>
<?php include 'Z_updates_bdd.php';?>
<?php include 'Z_fonctions_transversales.php';?>
	
<?php menu(var_menu_modif()); ?>

<div class="corps_modif">
	
<?php

// AFFICHAGE DE LA PAGE, SELON CE QU'ON Y FAIT : AJOUT, MODIFICATION, VÉRIFICATION *****************

echo verif_mdp(var_mdp());
var_verif_mdp();

if(isset($_SESSION['mdp']) && $_SESSION['mdp'] == var_mdp()) {
	
	// ENREGISTREMENT DES DONNÉES ******************************************************************

	if(isset($_POST['ajout_reussi']) || isset($_POST['modif_reussie']))
		update_insert($_POST);

	// Affichage ***********************************************************************************

	echo '<div class="modif_citation">
	
	<h1>Éléments à configurer</h1>';
	
	echo liste_deroulante_config();
	
	echo if_config('tag')
		. if_config('classement')
		. if_config('chapitre')
		. if_config('partie')
		. if_config('projet');

	echo if_nouvelle('citation', 'main')
		. if_nouvelle('oeuvre', 'main')
		
		. if_modif('citation', 'main')
		. if_modif('oeuvre', 'main');
		
	echo '</div></div>';
}

// FONCTIONS *******************************************************

// Initialisations

function selected($key, $fm, $label) {
	
	$b = '';
	$genre = $fm == 'f' ? 'nouvelle' : 'nouveau';
	$genre_art = $fm == 'f' ? 'une' : 'un';
	
	if(isset($_POST['type_config'])) {
		$b = $_POST['type_config'] == $key ? 'selected' : '';
	}
	elseif(isset($_POST[$genre . '_' . $key]) || isset($_POST['modif_' . $key])) {
		$b = 'selected';
	}
	elseif(isset($_POST['types_projets']) && $key == 'projet') $b = 'selected';
	
	return '<option value="' . $key . '" ' . $b . '>' . $label . '</option>';
}

function liste_deroulante_config() {
	
	return '<form method="post" action="modif.php">
		<select name="type_config">' 
			. selected('oeuvre', 'f', 'Enregistrer une œuvre')
			. selected('citation', 'f', 'Enregistrer une citation')
			. selected('tag', 'm', 'Modifier les tags')
			. selected('classement', 'm', 'Modifier les catégories de classement des tags')
			. selected('chapitre', 'm', 'Modifier les chapitres')
			. selected('partie', 'f', 'Modifier les parties')
			. selected('projet', 'm', 'Modifier les projets') . '
		</select>
	<input type="submit" class="chercher" value="Valider">
	</form>';
}

// FONCTIONS D'AFFICHAGE ***************************************************************************

// Nouvelle citation à enregistrer*****************************************************************

function if_nouvelle($type, $lieu)
{	
	if(isset($_POST['type_config']) && $_POST['type_config'] == 'oeuvre') $_POST['nouvelle_oeuvre'] = 1;
	if(isset($_POST['type_config']) && $_POST['type_config'] == 'citation') $_POST['nouvelle_citation'] = 1;
	if(isset($_POST['type_config']) && $_POST['type_config'] == 'projet') $_POST['nouveau_projet'] = 1;

	if(isset($_POST['nouvelle_' . $type]) || isset($_POST['nouveau_' . $type]))
	{
		$genre = isset($_POST['nouvelle_' . $type]) ? 'nouvelle' : 'nouveau';
		$lien = $lieu == 'demo' ? 'modif_demo' : 'modif';
		
		return '<h2>' . ucfirst($genre) . ' ' . $type . '</h2>
			
			<form method="post" action="' . $lien . '.php">'
				. liste_champs('', $type) . '			
				<input type="hidden" name="ajout_reussi"/>
				<input type="hidden" name="' . $genre . '_' . $type . '" value=1>
				<input class="chercher" type="submit" value="Enregistrer">
			</form>';
	}	
	else return '';
}

// Modifier une citation ou une œuvre**************************************************************
function if_modif($type, $lieu)
{
	global $bdd;
	if((isset($_GET['modif_' . $type]) && is_numeric($_GET['modif_' . $type])) || isset($_POST['modif_' . $type]) && is_numeric($_POST['modif_' . $type]))
	{	
		// Préparer les choses pour une œuvre, pour une citation, ou pour un projet ***************
		if($type == 'oeuvre' || $type == 'citation') {
			$var_sql = $_GET['modif_' . $type];
			$sql = modif_commande($type);
		}
		else {			
			$var_sql = modif_ID($type);
			$sql = modif_commande($type);
		}
		
		// Exécution de la requête*****************************************************************	
		$reponse = $bdd->prepare($sql);
		$reponse->execute(array($var_sql));

		// Affichage des données à modifier********************************************************
		while($donnees = $reponse->fetch(PDO::FETCH_ASSOC))
		{
			echo '<h2>Modifier l\'enregistrement</h2>';

			if($type == 'citation') $identifiant = 'ID_citation';
			elseif($type == 'oeuvre') $identifiant = 'ID_oeuvre';
			elseif($type == 'projet') $identifiant = 'ID_projet';
			elseif($type == 'classement') $identifiant = 'ID_classement';
			elseif($type == 'tag') $identifiant = 'ID_tag';
			elseif($type == 'partie' || $type == 'chapitre') $identifiant = 'ID_chapitre';
				
			if($type == 'citation') $label = 'la citation';
			elseif($type == 'oeuvre') $label = 'l\'œuvre (attention, cela entraînera la suppression de toutes les citations associées)';
			elseif($type == 'projet') $label = 'le projet';
			elseif($type == 'classement') $label = 'la catégorie de tag';
			elseif($type == 'partie') $label = 'la partie';
			elseif($type == 'chapitre') $label = 'le chapitre';
			elseif($type == 'tag') $label = 'le tag';
			
			$lien = $lieu == 'demo' ? 'modif_demo' : 'modif';
			echo '<form method="post" action="' . $lien . '.php">'
				. liste_champs($donnees, $type) . '	
				<input type="hidden" name="modif_reussie"/>	
				<input class="chercher" type="submit" value="Enregistrer les modifications"/>
			</form>';
			
			echo '<form method="post" action="modif.php">
				<input type="hidden" name="suppr_' . $type . '" />
				<input type="hidden" name="ID_' . $type . '" value="' . $donnees[$identifiant] . '"/>
				<input type="submit" class="chercher" value="Ou supprimer ' . $label . '"/>
			</form>';
		}
		$reponse->closeCursor();	 	
	}
}

// Modifier des éléments de structuration de Litote *****************
function if_config($type){
	
	global $bdd;
	$a = '';
		
	// Si une action est requise sur les tags	
	if($type == 'tag') {

		if(((isset($_POST['type_config']) && $_POST['type_config'] == $type) || (isset($_GET['type_config']) && $_GET['type_config'] == $type && !isset($_GET['ID_tag']))
			&& (!isset($_POST['nouveau_tag']) || !isset($_POST['modif_tag']))))
		{
			$a = '<form method="post" action="modif.php">';
			
			// Initialisation du formulaire
			
			$a .= '<br><br><label for="liste_tags">Liste des tags&#8239;:<br></label>
					<select id="liste_tags" name="ID_tag">';
			
			$reponse = $bdd->query('SELECT * FROM tags ORDER BY tag_complet');
			while($donnees = $reponse->fetch())
			{			
				$b = isset($_POST['liste_tags']) && $_POST['liste_tags'] == $donnees['ID_tag'] ? 'selected' : '';
				
				$a .= '<option value="' . $donnees['ID_tag'] . '" ' . $b . '>' . ucfirst($donnees['tag_complet']) . '</option>';
			}
			$reponse->closeCursor();		
			
			$a .= '</select><br><br> '
				. modif_suppr_ou_ajout('tag', 'le tag', '');
		}
		
		$a .= formulaires($type, 'nouveau');	
	}
	
	// Si action requise sur les classements	
	if($type == 'classement') {
		
		// Initiatisation du formulaire
		if((isset($_POST['type_config']) && $_POST['type_config'] == 'classement'))
		{	
			$a = '<form method="post" action="modif.php">
				<select name="ID_classement">';
		
			$reponse = $bdd->query('SELECT * FROM classementtags ORDER BY ordre');
			while($don = $reponse->fetch())
			{
				$b = isset($_POST['ID_classement']) && $_POST['ID_classement'] == $don['ID_classement'] ? 'selected' : '';
				
				$a .= '<option value="' . $don['ID_classement'] . '" ' . $b . '>' . $don['ordre'] . ' - ' . $don['titre']. '</option>';
			}
			$reponse->closeCursor();
			
			$a .= '</select><br><br>' . 
					modif_suppr_ou_ajout($type, 'le classement de tags', '');
		}
		
		$a .= formulaires($type, 'nouveau');		
	}
	
	// Si action requise sur les chapitres
	if($type == 'chapitre') {
		
		//Initialisation du formulaire		
		if((isset($_POST['type_config']) && $_POST['type_config'] == 'chapitre') 
		&& !isset($_POST['nouveau_chapitre'])
		&& !isset($_POST['modif_chapitre'])
		|| (isset($_GET['type_config']) && $_GET['type_config'] == 'chapitre' && !isset($_GET['ID_chapitre'])))
		{				
			$a = '<form method="post" action="modif.php">
				<select name="ID_chapitre">';
			
			$reponse = $bdd->query('SELECT * FROM chapitres 
				WHERE chap NOT LIKE \'partie%\' ORDER BY titre');
			while($donnees = $reponse->fetch())
			{
				$a .= '<option value="' . $donnees['ID_chapitre'] . '">' . ucfirst($donnees['titre']) . '</option>';
			}
			$reponse->closeCursor();
			
			$a .= '</select><br><br>' .
				modif_suppr_ou_ajout('chapitre', 'le chapitre', '');
		}
		
		$a .= formulaires($type, 'nouveau');
	}
	
	// Si action requise sur les parties
	if($type == 'partie') {
		
		// Initialisation du formulaire
		if((isset($_POST['type_config']) && $_POST['type_config'] == 'partie') && !isset($_POST['nouvelle_partie']) && !isset($_POST['modif_partie']))
		{
			$a = '<br><form method="post" action="modif.php">
				<select name="ID_chapitre">';
				
			$reponse = $bdd->query('SELECT * FROM chapitres WHERE chap = "partie" ORDER BY partie');
			while($donnees = $reponse->fetch())
			{
				$a .= '<option value="' . $donnees['ID_chapitre'] . '">' . $donnees['partie'] . ' - ' . $donnees['titre'] . '</option>';
			}
			$reponse->closeCursor();
			
			$a .= '</select><br><br>' .
				modif_suppr_ou_ajout('partie', 'la partie', 'e');
		}
		
		$a .= formulaires($type, 'nouvelle');
	}
	
	// Si action requise sur les projets
	if($type == 'projet') {
		
		// Initialisation du formulaire
		if(isset($_POST['types_projets'])
			|| (isset($_POST['type_config']) && $_POST['type_config'] == 'projet')
			&& (!isset($_POST['nouveau_projet']) && !isset($_POST['modif_projet'])))
		{
			
			$a = '<br><form method="post" action="modif.php">
				<label>Type de projet :</label>
				<br><select name="types_projets">';
				
			$array = array('publication', 'communication', 'brouillon', 'annexe');
			
			foreach ($array as $value) {
				
				$b = isset($_POST['types_projets']) && $_POST['types_projets'] == $value ? 'selected' : '';
				
				$a .= '<option value="' . $value . '" ' . $b . '>' . ucfirst($value) . '</option>';				
			}
			
			$a .= '</select>
				<br><input type="submit" class="chercher" value="Valider la sélection">
				</form>
				
				<form method="post" action="modif.php">
				<input type="hidden" name="nouveau_projet" value=1>
				<input type="submit" value="Ou ajouter un autre projet" class="chercher">
				</form>';
		}

		if(isset($_POST['types_projets'])) {
				
			$a .= '<br><form method="post" action="modif.php">
			<select name="ID_projet">';
			
			$reponse = $bdd->query('SELECT * FROM projets WHERE type="' . $_POST['types_projets'] . '"');
			while($donnees = $reponse->fetch())
			{
				$b = isset($_POST['ID_projet']) && $_POST['ID_projet'] == $donnees['ID_projet'] ? 'selected' : '';
				
				$a .= '<option value="' . $donnees['ID_projet'] . '" ' . $b . '>' . $donnees['titre'] . '</option>';
			}
			$reponse->closeCursor();
			
			$a .= '</select><br><br>' . 
				modif_suppr_ou_ajout('projet', 'le projet', '');
		}
		
		$a .= formulaires($type, 'nouveau');
	}
	
	return $a;
}

// Appelée dans tous les cas : affiche les labels et les champs à remplir **************************

function liste_champs($donnees, $type)
{
	global $bdd;
	$a ='';
	
	//~ // S'il s'agit d'un projet à créer ou à modifier ***********************************************
	if($type == 'projet')
	{
		$infobulle_date = infobulle('La date doit être inscrite au format AAAAMM. JJ éventuellement à la suite, mais l\'affichage sera moins harmonieux !');
		
		$infobulle_reference = infobulle('La case peut rester vide. La référence bibliographique doit être enregistrée en markdown : _tirets underscore_ pour l\'italique, **doubles étoiles** pour les gras, crochets et guillemets pour les liens [comme ceci, là c\'est le texte qui fait lien](et là c\'est l\'adresse à quoi il renvoie).');
		
		$infobulle_chapitre = infobulle('Si le nouveau projet est une annexe de la thèse, inscrire le numéro du chapitre dans lequel elle est convoquée. Sinon ne rien écrire.');
		
		$value = isset($donnees['ID_projet']) ? $donnees['ID_projet'] : '';

		$a = m_input($donnees, 'titre', 'Titre', '70', '1') . '<br>' 
			. m_input($donnees, 'date', 'Date ' . $infobulle_date) . '<br>'
			. m_input($donnees, 'chap', 'Chapitre ' .$infobulle_chapitre) . '<br>
			
			<label for="type">Type : </label>
			<select name="type">';

			$options = array('brouillon', 'annexe', 'communication', 'publication');
			
			foreach($options as &$option) {
				
				$b = isset($donnees['type']) && $donnees['type'] == $option? 'selected' : '';
				
				$a .= '<option value="' . $option . '" ' . $b . '>' . ucfirst($option) . '</option>';
			}
			
			$a .= '</select><br>' 
			
			. m_textarea($donnees, 'ref_biblio', 'Référence bibliographique ' . $infobulle_reference, '70', '4') . ' 
			<input type="hidden" name="ID_projet" value="' . $value . '"/>';
	}
	
	//~ // S'il s'agit d'une œuvre à créer ou à modifier************************************************
	if($type == 'oeuvre')
	{
		$infobulle['nom'] = infobulle('S\'il y a plusieurs autrices, indiquer leurs noms en les séparant par un point-virgule, en veillant à respecter le même ordre que pour les prénoms !');
		$infobulle['prenom'] = infobulle('S\'il y a plusieurs autrices, indiquer leurs prénoms en les séparant par un point virgule.');
		$infobulle['notes'] = infobulle('Utiliser le markdown. ' . explications_markdown());
		$infobulle['corpus'] = infobulle('Par défaut, l\'œuvre est présentée comme appartenant au corpus secondaire, majoritaire : c\'est-à-dire qu\'elle est dans les clous de la définition du corpus de thèse, mais qu\'elle ne fait pas partie du noyau d\'œuvres auxquelles celle-ci s\'intéressera prioritairement.');
		
		$value = isset($donnees['ID_oeuvre']) ? $donnees['ID_oeuvre'] : '';

		$a = m_input($donnees, 'prenom', 'Prénom' . $infobulle['nom'])
			. m_input($donnees, 'nom', 'Nom' . $infobulle['prenom'])
			. m_input($donnees, 'nationalite', 'Pays ou espace culturel')
			. m_input($donnees, 'titre', 'Titre')
			. var_champs_modif_corpus($donnees, $infobulle['corpus'])
			. m_input($donnees, 'genre_litt', 'Genre littéraire identifié')	
			. m_input($donnees, 'origine_date', 'Date de première publication')
			. m_input($donnees, 'origine_edition', 'Édition de première publication')
			. m_input($donnees, 'origine_lieu', 'Lieu de première publication')
			. m_input($donnees, 'courant_date', 'Date de l\'édition consultée')
			. m_input($donnees, 'courant_edition', 'Maison de l\'édition consultée')
			. m_input($donnees, 'courant_lieu', 'Lieu de l\'édition consultée')	
			. var_champs_modif_booleens($donnees)
			. var_champs_modif_inputs($donnees)
			. m_textarea($donnees, 'reflexions', 'Notes diverses sur l\'œuvre' . $infobulle['notes'], '70', '10')
			. m_textarea($donnees, 'resume', 'Résumé de l\'œuvre' . $infobulle['notes'], '125', '40') . '
			<input type="hidden" name="ID_oeuvre" value="' . $value . '"/>';
	}
	
	//~ // S'il s'agit d'une citation à créer ou à modifier*********************************************
	if($type == 'citation')
	{
		$infobulle_titre = infobulle('Par défaut, c\'est le titre correspondant à la dernière citation enregistrée qui s\'affiche.');
		$infobulle_notes = infobulle('Utiliser le markdown. ' . explications_markdown() . ' D\'autres éléments seront automatiquement transformés dans la base de données : les guillemets seront transformés en guillemets anglais, les apostrophes droites seront transformées en apostrophes courbes.');
		$infobulle_page = infobulle('Numéro simple ou intervalle de pages. Exemples : 143, 143-144.');
		$infobulle_ref_these = infobulle('Paginaton de référence dans la thèse. Numéro simple ou intervalle de pages. Exemples : 143, 143-144.');
		$infobulle_url_these = infobulle('Url de lien vers la thèse, idéalement vers l\'endroit exact où la citation est mobilisée.');
		
		$value = isset($donnees['ID_citation']) ? $donnees['ID_citation'] : '';
		
		// Sélectionner l'œuvre, par défaut la dernière choisie ************************************
		$a = '<label>Titre de l\'œuvre ' . $infobulle_titre . ' : </label><select name="ID_oeuvre">';
	
		$rep = $bdd->query('SELECT ID_oeuvre,ID_citation FROM citations ORDER BY ID_citation DESC LIMIT 1');
		$d_cit = $rep->fetch();
		$rep->closeCursor();
		
		$reponse = $bdd->query('SELECT titre,ID_oeuvre FROM oeuvres ORDER BY titre');
		while($don = $reponse->fetch())
		{
			if(isset($donnees['ID_oeuvre']) && $donnees['ID_oeuvre'] == $don['ID_oeuvre'])
			{	
				$c = 'selected';
			}
			elseif($d_cit['ID_oeuvre'] == $don['ID_oeuvre'] && isset($_POST['nouvelle_citation']))
			{
				$c = 'selected';
			}
			else $c = '';
			
			$a .= '<option value=' . $don['ID_oeuvre'] . ' ' . $c . '>' . $don['titre'] . '</option>';
		}
		$reponse->closeCursor();	
			
		$a .= '</select><br>';
		
		// Afficher les champs citation, page, contexte*********************************************
		
		$a .= m_textarea($donnees, 'citation', 'Citation ' . $infobulle_notes, '125', '10')
			. m_input($donnees, 'page', 'Numéro de page ' . $infobulle_page)
			. m_textarea($donnees, 'contexte', 'Notes ' . $infobulle_notes, '80', '1');
			
		// Afficher les tags ***********************************************************************
		
		$a .= '<br><div class="tags3">';

		$classement = $bdd->query('SELECT * FROM classementtags ORDER BY ordre');
		while($class = $classement->fetch(PDO::FETCH_ASSOC))
		{
			$a .= '<strong>' . $class['titre'] . '</strong><br>';
			
			$reponse = $bdd->query('SELECT tag_complet, tag_bdd FROM tags WHERE ID_classement =' . $class['ID_classement'] . ' ORDER BY tag_complet');
			while($don = $reponse->fetch(PDO::FETCH_ASSOC))
			{
				$a .= m_checkbox($donnees, $don['tag_bdd'], ucfirst($don['tag_complet'])) . '<br>';
			}
			$reponse->closeCursor();
		}
		$classement->closeCursor();


		
		$a .= '</div><input type="hidden" name="ID_citation" value="' . $value . '"/>';
		
		// Afficher les chapitres ******************************************************************
		
		$a .= '<div class="chapitres">';
		
		// introduction fixe		
		$a .= enreg_chapitres($bdd, 'chap', $donnees, 'intro');
		
		// chapitres et parties selon projets
		$reponse = $bdd->query('SELECT titre, partie FROM chapitres WHERE chap="partie" ORDER BY partie');
		while($don = $reponse->fetch())
		{
			$a .= '<br><br><br><strong>' . $don['titre'] . '</strong><br>';
			
			$reponse2 = $bdd->query('SELECT * FROM chapitres WHERE chap="chapitre" AND partie="' . $don['partie'] . '" ORDER BY titre');
			while($do = $reponse2->fetch())
			{
				$a .= enreg_chapitres($bdd, 'chap', $donnees, $do);
			}
			$reponse2->closeCursor();
		}
		$reponse->closeCursor();
				
		// conclusion et annexes fixes
		$a .= '<br>' . enreg_chapitres($bdd, 'chap', $donnees, 'conclu');
			
		$a .= '<br>' . enreg_chapitres($bdd, 'chap', $donnees, 'annexes');
			
		$a .= '<input type="hidden" name="ID_citation" value="' . $value . '"/><br>';
				
		// Afficher les autres projets possibles ***************************************************
		
		$a .= '<div class="projets">
		
			<br><strong>Autres projets ?</strong><br>'
			 . enreg_chapitres($bdd, 'projets', $donnees, '') . '</div>';
	}
	
	// S'il s'agit d'un tag à créer ou à modifier*********************************************
	if($type == 'tag')
	{
		
		$a = m_textarea($donnees, 'tag_complet', 'Titre complet', '80', '1');
		
		if(!isset($_POST['ID_tag']) && !isset($_GET['ID_tag'])) $a .= m_input($donnees, 'tag_bdd_required', 'Titre court sans caractère spécial, ni accent, ni espace, ni majuscule');
			
		$a .= '<br><label for="classement">Sous-catégorie de classement&#8239;:</label>
			<select id="classement" name="ID_classement">';
			
			$reponse2 = $bdd->query('SELECT * FROM classementtags ORDER BY ordre');
			while($class = $reponse2->fetch())
			{			
				$b = isset($donnees['ID_classement']) && $donnees['ID_classement'] == $class['ID_classement'] ? 'selected' : '';
				
				$a .= '<option value="' . $class['ID_classement'] . '" ' . $b . '>' . ucfirst($class['titre']) . '</option>';
			}
			$reponse2->closeCursor();				
			
			$a .= '</select><br>';
			
		if(isset($donnees['ID_tag'])) $a .= '<input type="hidden" name="ID_tag" value="' . $donnees['ID_tag'] . '">';
		
		if(isset($donnees['ID_tag'])) $a .= '<input type="hidden" name="ancien_tag_complet" value="' . $donnees['tag_complet'] . '">';
	}
	
	// S'il s'agit d'un classement de tag à créer ou à modifier*********************************************
	if($type == 'classement')
	{
		$a = m_textarea($donnees, 'titre', 'Titre complet', '60', '1')
			. m_input($donnees, 'ordre', 'Ordre d\'affichage');
		
		if(isset($donnees['ID_classement'])) $a .= '<input type="hidden" name="ID_classement" value="' . $donnees['ID_classement'] . '">';
	}
	
	// S'il s'agit d'un chapitre à créer ou à modifier*********************************************
	if($type == 'chapitre')
	{
		$infobulle_partie = infobulle('Inscrire en chiffre arabe simple le numéro de la partie à laquelle correspond le chapitre : 1, 2, 3, 4...');		
		$infobulle_titre_court = infobulle('Format conseillé : « chapitre 1 », « chapitre 2 »...');
		
		$a = m_textarea($donnees, 'titre', 'Titre complet', '60', '1') 
		. '<input type="hidden" name="chap" value="chapitre">';
		
		if(isset($_POST['nouveau_chapitre'])
			|| ((isset($_POST['modif_chapitre']) || isset($_GET['modif_chapitre']))
				&& $donnees['chap'] != 'intro'
				&& $donnees['chap'] != 'conclu'
				&& $donnees['chap'] != 'annexes'))
		{
			$a .= m_input($donnees, 'titre_court', 'Titre court' . $infobulle_titre_court)
			. m_input($donnees, 'partie', 'Inscrit dans la partie' . $infobulle_partie);
		}
			
		if(isset($donnees['ID_chapitre'])) $a .= '<input type="hidden" name="ID_chapitre" value="' . $donnees['ID_chapitre'] . '">';
	}
	
	// S'il s'agit d'une partie à créer ou à modifier*********************************************
	if($type == 'partie')
	{
		$infobulle_numero_partie = infobulle('Inscrire en chiffre arabe simple le numéro de la partie : 1, 2, 3, 4...');		
		
		$a = m_textarea($donnees, 'titre', 'Titre complet', '80', '2')
			. m_input($donnees, 'partie', 'Numéro' . $infobulle_numero_partie) . '
			<input type="hidden" name="chap" value="partie">
			<input type="hidden" name="titre_court" value="-">';
			
		if(isset($donnees['ID_chapitre'])) $a .= '<input type="hidden" name="ID_chapitre" value="' . $donnees['ID_chapitre'] . '">'; 
	}
		
	return $a;
}

function enreg_chapitres($bdd, $type, $citation, $condition)
{
	$a = '';
	
	if($type == 'projets') {
		$reponse = $bdd->query('SELECT titre, ID_projet FROM projets WHERE type = "brouillon" ORDER BY type, date DESC');
		while($don = $reponse->fetch(PDO::FETCH_ASSOC))
		{
			$a .= m_checkbox($citation, $don['ID_projet'], $don['titre']) . '<br>';
		}
		$reponse->closeCursor();
	}
	
	elseif($type == 'chap') {
		
		if($condition == 'intro' || $condition == 'conclu' || $condition == 'annexes') {
			$reponse = $bdd->query('SELECT * FROM chapitres WHERE chap="' . $condition . '" ORDER BY titre');
			while($chapitre = $reponse->fetch(PDO::FETCH_ASSOC))
			{
				$titre = '<strong>' . ucfirst($chapitre['titre']) . '</strong>';
				
				$a .= localisation_chapitres('chap' . $chapitre['ID_chapitre'], $titre, $condition, $chapitre, $citation);
			}
			$reponse->closeCursor();
		}
		
		else {		
			
			$titre = ucfirst($condition['titre']);
			
			$a .= localisation_chapitres('chap' . $condition['ID_chapitre'], $titre, $condition, $condition, $citation);
		}
		
	}
	
	return $a;
}

function localisation_chapitres($id, $titre, $condition, $chapitre, $citation) {
		
	$value = isset($citation[$id]) && $citation[$id] != 0 ? ' checked' : '';
	$valueb = isset($citation[$id . '_ver']) && $citation[$id . '_ver'] != 0 ? ' checked' : '';
	
	$cit_ver = $citation != '' ? $citation[$id . '_ver'] : '';
	$cit_ver_url = $citation != '' ? $citation[$id . '_ver_url'] : '';
	
	return var_modif_stock($titre, $id, $value, $valueb);
}

// Appelée dans if_config () : propose trois solutions d'enregistrement *******************

function modif_suppr_ou_ajout($type, $label, $accord) {
				
	$nouveau = $accord == 'e' ? 'nouvelle' : 'nouveau';
	
	return '
	<label for="modifier">Modifier ' . $label . ' sélectionné' . $accord . '&#8239;:</label>
	<input type="checkbox" name="modif_' . $type . '" value=1 id="modifier">

	<br><label for="supprimer">Supprimer ' . $label . ' sélectionné' . $accord . '&#8239;:</label>
	<input type="checkbox" name="suppr_' . $type . '" id="supprimer">

	<br><input type="submit" value="Valider la sélection" class="chercher">
	</form>

	<form method="post" action="modif.php">
	<input type="hidden" name="' . $nouveau . '_' . $type . '" value=1>
	<input type="submit" value="Ou ajouter un' . $accord . ' autre ' . $type . '" class="chercher">
	</form>';
}

// Appelée dans if_config() : affiche les formulaires

function formulaires($type, $accord) {
	
	$a ='';
	
	// Dans le cas d'un ajout
	if(isset($_POST[$accord . '_' . $type])) {
		$a = if_nouvelle($type, 'main');
	}
	
	// Dans le cas d'une modification			
	if(isset($_POST['modif_' . $type]) || isset($_GET['modif_' . $type])) 
	{
		$a = if_modif($type, 'main');
	}
	
	return $a;
}

// Appelée dans liste_champs() : met en forme les champs de texte court*****************************

function m_input($donnees, $bdd, $label)
{
	$string = $bdd;
	$req = '';
	if(str_contains($string, 'required')) {
		$req = ' required';
		$bdd = str_replace('_required', '', $string);
	}
	
	if(substr($bdd, -3) == 'ver') $class =  'cite_dans_page';
	elseif(substr($bdd, -3) == 'url') $class = 'url_citation';
	else $class = 'm_' . $bdd;
	
	$value = $donnees != '' ? 'value="' . $donnees[$bdd] . '"' : '';
	
	return '<label>' . $label . '&#8239;: </label>
		<input class="' . $class . '" name="' . $bdd . '" type="text" ' . $value . $req . '/><br>';
}

// Appelée dans liste_champs() : met en forme les champs de texte long *****************************

function m_textarea($donnees, $bdd, $label, $cols, $rows)
{
	$value = $donnees != '' ? $donnees[$bdd] : '';
	return '<br><label>' . $label . '&#8239;: </label><br>
	<textarea rows="' . $rows . '" cols="' . $cols . '" class="m_' . $bdd . '" name="' . $bdd . '">' . $value . '</textarea><br>';
}

// Appelée dans liste_champs() : met en forme les cases à cocher ***********************************

function m_checkbox($donnees, $tag, $label)
{
	$name = is_numeric($tag) ? 'projet' . $tag : $tag;
	$value = ($donnees != '' && $donnees[$name] == 1) ? ' checked' : '';
	return '<input type="checkbox" value="1" name="' . $name . '"' . $value . '/>' . ucfirst($label);
}

//~ function m_input_verrouille($donnees, $bdd, $label)
//~ {
	//~ if(!str_contains($bdd, 'partie')) {
		
		//~ $value = $donnees != '' ? 'value="' . $donnees[$bdd] . '"' : '';
		//~ $url = $donnees != '' ? 'value="' . $donnees[$bdd . '_url'] . '"' : '';
	
		//~ return '<div class="cite_dans">
		
		 //~ <label>' . $label . '</label>
		//~ <input class="cite_dans_page" name="' . $bdd . '" type="text" ' . $value . '/>
		
		//~ <label>Url:</label>
		//~ <input class="url_citation" name="' . $bdd . '_url" type="text"' . $url . '/>
		//~ </div>';
	//~ }
//~ }

// Appelée dans liste_champs() : met en forme les choix à liste finie*******************************

function m_select($donnees, $tag, $label)
{
	global $bdd;
	
	$a = $label . ' : <select name="' . $tag . '">
		<option value="null">' . $label .'</option>';
			
		$reponse = $bdd->query('SELECT DISTINCT ' . $tag . ' FROM oeuvres ORDER BY ' . $tag);
		while($fetch = $reponse->fetch())
		{
			$value = ($donnees != '' && $donnees[$tag] == $fetch[$tag]) ? ' selected' : '';
			$a .= '<option value="' . $fetch[$tag] . '"' . $value . '>' . $fetch[$tag] . '</option>';
		}
		$reponse->closeCursor();	
			
		$a .= '</select><br>';
	return $a;
}

function modif_ID($type) {
			
	if($type == 'partie' && isset($_POST['ID_chapitre'])) return $_POST['ID_chapitre'];
	elseif($type == 'partie' && isset($_GET['ID_chapitre'])) return $_GET['ID_chapitre'];
	
	else $id = 'ID_' . $type;
	
	if(isset($_POST[$id])) return $_POST[$id];
	
	else return $_GET[$id];
}

function modif_commande($type) {
	
	if($type == 'classement') $table = 'classementtags';
	elseif($type == 'partie') $table = 'chapitres';
	else $table = $type . 's';
	
	if($type == 'partie') $id = 'ID_chapitre';
	else $id = 'ID_' . $type;
		
	return 'SELECT * FROM ' . $table . ' WHERE ' . $id . '= ?';
}

?>

</div>

</div>

<?php echo footer(); ?>

</body>

</html>
