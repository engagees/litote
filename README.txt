Bonjour !

Voici le code PHP associé à Litote, application web de gestion de corpus et d'aide à la rédaction, pensée essentiellement pour les besoins de longs travaux de recherche en humanités, sur une base textuelle.
-> Version démonstration avec explications : https://litteratures-engagees-feminisme.huma-num.fr/

Sur le dépôt : 
- Les fichiers php à associer à un serveur et à un nom de domaine ;
- Les fichiers css de stylage ;
- La structure de la base de données associée.

Je code en totale amatrice, ça n'est pas très optimisé ; en revanche j'ai fait en sorte de commenter un max pour qu'on puisse s'y retrouver aisément.


Pour installer :

1. S'assurer d'avoir un serveur à disposition, distant ou local.

2. Importer les tables dans MySql ou équivalent.
    -> Attention, la table oeuvres et la table citations sont susceptibles d'être structurées sensiblement différemment selon les besoins des projets. Ici je dépose la version de départ, qui fonctionne sur des données éditoriales simples (éditeur, pays, genre littéraire, etc.) ; la plupart des autres projets auront besoin de changer le nom et le fonctionnement de deux trois colonnes.

3. La plupart des fichiers sont utilisables tels quels, les variations sont réunies dans des fichiers à part :
    - style_PROJET_SPECIFIQUE.css (réunit variations de styles, principalement couleurs)
    - Z_fonctions_variations.php (réunit toutes les variations anticipées dans le code)
    - Z_meta_variations.php (si besoin de changer quelques données méta sur certaines pages)

Ces listes de variations sont établies par l'expérience de quatre différents projets Litote, mais il n'est pas dit qu'elles couvrent tout ce qui pourrait devoir varier dans un autre projet. Pour exemple de ces variations, je renvoie aux autres modèles mis à disposition sur la page de démo (à venir).
Idéalement, Litote pourrait devenir un logiciel gérant de manière fluide ces variations de projets.

TUTO COMPLET PAR ICI : (https://engagees.hypotheses.org/4523)[https://engagees.hypotheses.org/4523].



TO DO :
- Adaptation responsive
- Code à optimiser par fonctions pour certaines répétitions
