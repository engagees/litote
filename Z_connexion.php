<?php

	// Afficher les erreurs à l'écran
    ini_set('display_errors', 1);
    // Afficher les erreurs et les avertissements
    error_reporting(E_ALL);

// ACCÈS À LA BASE DE DONNÉES ************************************************************************

$serveur = var_connexion_serveur();
$identifiant = var_connexion_identifiant();
$dbname = var_connexion_dbname();
$mdp = var_connexion_mdp();

$bdd = var_methode_connexion($serveur, $dbname, $identifiant, $mdp);

// Appelée sur différentes pages : vérifie si besoin de connexion ***********************************
function verif_mdp($mdp) {
	if((!isset($_SESSION['mdp']) || $_SESSION['mdp'] != $mdp)
		&& (!isset($_POST['mdp']) || $_POST['mdp'] != $mdp))
	{
		return '<div class="modif_citation">
		<form method="post" action="modif.php">
			<label>Mot de passe : </label><input type="text" name="mdp"/>
			<input type="submit" value="Connexion"/>
		</form>
		
		<p>Attention, si vous étiez en train de préparer une mise à jour des données, il est possible que vous deviez recommencer l\'opération après vous être connecté·e.</p>
		
		</div>';
	}
}
?>
